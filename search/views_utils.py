# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.


"""
Contains utilities used in views. Mostly:

- a function to update a card by searching on Dilicom
  - a function to do bulk imports with Dilicom (batches of 100s)
- by searching on Electre
- by searching on the default datasource.
  - or on the appropriate datasource, given the book's lang.

The search functions that make HTTP requests have a simbling function with a cache of 20 hours.
Indeed, Dilicom's "FEl à la demande par web service" is limited to 40,000 requests per year.
Electre has limitations too.
So a client can bypass its limits and pay extra credits, without prior notice by the vendor.
(We should count the number of requests on our side)

Abelujo wants to update a book data when the user consults a book page, but we cache the results for some hours.
We want(ed) to get the last book data when we sell it, to see a possible price change, but that proved too costly, 40,000 is not much.

See also the dev docs.
"""

from __future__ import unicode_literals

import os
import unicodecsv
import termcolor
import re
from cachetools import cached, TTLCache

from django.utils.translation import ugettext as _

# Datasources: also in views and searchResultsController.
from search.models.utils import is_isbn
from search.datasources.bookshops.all.discogs import discogsScraper as discogs  # noqa: F401
from search.datasources.bookshops.all.momox import momox  # noqa: F401
from search.datasources.bookshops.deDE.buchlentner import \
    buchlentnerScraper as buchlentner  # noqa: F401
from search.datasources.bookshops.esES.agapea import \
    agapeaScraper as agapea  # noqa: F401
from search.datasources.bookshops.frFR.librairiedeparis import \
    librairiedeparisScraper as librairiedeparis  # noqa: F401
from search.datasources.bookshops.frFR.chezmonlibraire import \
    chezmonlibraireScraper as chezmonlibraire  # noqa: F401
from search.datasources.bookshops.frFR.dilicom import \
    dilicomScraper as dilicom  # noqa: F401
from search.datasources.bookshops.frFR.lelivre import \
    lelivreScraper as lelivre  # noqa: F401
from search.datasources.bookshops.frFR.filigranes import \
    filigranesScraper as filigranes  # noqa: F401
from search.datasources.bookshops.all.bookdepository import \
    bookdepositoryScraper as bookdepository  # noqa: F401
from search.models import Card
from search.models import get_distributor_from_gln
from search.datasources.bookshops.itIT.hoeplit import hoeplitScraper as hoeplit  # noqa: F401
from search.datasources.bookshops.nlNL.athenaeum import athenaeumScraper as athenaeum  # noqa: F401
from search.datasources.bookshops.nlNL.naibooksellers import naibooksellersScraper as naibooksellers  # noqa: F401
from search.datasources.bookshops.all.abebooks import abebooksScraper as abebooks  # noqa: F401
from search.datasources.bookshops.all.bookfinder import bookfinderScraper as bookfinder  # noqa: F401

# Extra, optional and private scripts:
try:
    from abextras import bertrandScraper as bertrand  # noqa: F401
    print("abextras scripts are loaded.")
except ImportError:
    # that's ok.
    pass

from abelujo import settings

from search.models import utils
from search.models import Distributor

try:
    # The Electre API connector is installed separetely.
    import pyelectre
except:
    print("ABELUJO INFO: the pyelectre module is not installed (and that's OK).")
    pass

log = utils.get_logger()

#: Default datasource to be used when searching isbn, if source not supplied.
DEFAULT_DATASOURCE = "librairiedeparis"
if os.getenv('DILICOM_PASSWORD') and os.getenv('DILICOM_PASSWORD').strip():
    DEFAULT_DATASOURCE = 'dilicom'
    print(termcolor.colored('default datasource is DILICOM', 'blue'))

def electre_enabled():
    return os.getenv('ELECTRE_PASSWORD') is not None \
        and os.getenv('ELECTRE_USER') is not None

if electre_enabled():
    DEFAULT_DATASOURCE = 'electre'
    print(termcolor.colored('default datasource is ELECTRE', 'blue'))

if os.getenv('DEFAULT_DATASOURCE') and os.getenv('DEFAULT_DATASOURCE').strip():
    DEFAULT_DATASOURCE = os.getenv('DEFAULT_DATASOURCE')

def get_datasource_from_lang(lang):
    """
    From a lang (str, coming from the user's URL), return the name (str) of the datasource module.

    BUT: this works fine when /fr/ leads to the french scraper, when /es/ leads to the spanish one.
    However, users starts working with multiple languages now. So, we now pass the datasource parameter from their search request.
    """
    if os.getenv('DEFAULT_DATASOURCE'):
        # Required to set the swiss source by default over the french one.
        return os.getenv('DEFAULT_DATASOURCE')

    if not lang:
        return DEFAULT_DATASOURCE

    if lang.startswith("fr") and not os.getenv('DILICOM_PASSWORD'):
        return "librairiedeparis"
    elif lang.startswith("fr") and os.getenv('DILICOM_PASSWORD'):
        return "dilicom"
    elif lang.startswith('ch'):
        return 'lelivre'
    elif lang.startswith("de"):
        return "buchlentner"
    elif lang.startswith("es"):
        return "agapea"
    elif lang.lower().startswith("it"):
        return "hoeplit"
    elif lang.lower().startswith("pt"):
        return "bertrand"
    elif lang in ["discogs", "momox"]:
        return lang
    else:
        return DEFAULT_DATASOURCE

def is_official_datasource(source):
    """
    Is that source name (str) an official provider?
    Current official providers: Dilicom, Electre.

    The difference is that we have a real web service and important
    data to update regularly.
    """
    return source.lower() in ['dilicom', 'electre']

@cached(cache=TTLCache(maxsize=1024, ttl=72000))  # 20 hours
def search_on_data_source_with_cache(data_source, search_terms, PAGE=1):
    """
    Cache the results for 20 hours.

    NOTE: it's a memory cache, so every time the webserver is
    restarted the cache is reset.
    """
    return search_on_data_source(data_source, search_terms, PAGE=PAGE)

def search_on_data_source(data_source, search_terms, PAGE=1):
    """
    Search with the appropriate scraper.

    - data_source is the name of an imported module.  Electre takes
      priority (it supports EAN search and free search, but not search of
      many EANs).
    - search_terms: list of strings.

    Return: a tuple (search results, stacktraces). Search result is a
    list of dicts, stacktraces a list.
    """
    if data_source == 'electre':
        if electre_enabled():
            res, traces = pyelectre.search_on_electre(search_terms)
        else:
            return [], ["Electre search is not enabled."]
    else:
        # get the imported module by name.
        # They all must have a class Scraper.
        scraper = getattr(globals()[data_source], "Scraper")
        # res, traces = scraper(*search_terms).search()
        res, traces = scraper(search_terms, PAGE=PAGE).search()

    # Check if we already have these cards in stock (with isbn).
    res = Card.is_in_stock(res)

    return res, traces


def postSearch(card_obj):
    """
    Call the postSearch function of the module imported with the name
    of data_source.

    data_source: a scraper name, containing a function postSearch
    details_url: url of the card's details.

    return: a dict of results (for books, must have the isbn/ean).
    """
    if not card_obj:
        log.info("calling postSearch: no card obj: do nothing.")
        return
    if not card_obj.details_url:
        log.info("calling postSearch: no details url: do nothing.")
        return
    try:
        card_dict = {
            "isbn": card_obj.isbn,
            "details_url": card_obj.details_url,
        }
        # Allow a scraper not to have the postSearch method.
        res = getattr(globals()[card_obj.data_source], "postSearch")(
            card_obj.details_url,
            isbn=card_obj.isbn,
            card=card_dict,
        )
        if res:
            # XXX: quite long :/
            Card.update_from_dict(card_obj, card_dict)

    except Exception as e:
        log.warning(e)
        res = {}
    return res


def dilicom_enabled():
    """
    Return a boolean indicating if the connection to Dilicom is possible.
    Currently, check we have the required environment variables.
    """
    return os.getenv('DILICOM_PASSWORD') is not None \
        and os.getenv('DILICOM_USER') is not None


def update_from_dilicom_with_cache(card):
    res, messages = _call_update_from_dilicom_with_cache(card)

    # Update only the parameters taken from Dilicom.
    # A card object has more data in it (last_soldcard_date, shelf that could be updated…)
    # so return the card, not the result from the cache.

    # <2024-04-04> Update price or respect "update price in sell" setting?
    # card.price = res.price
    card.availability = res.availability
    card.availability_fmt = res.availability_fmt
    card.img = res.img
    card.vat = res.vat
    card.theme = res.theme
    # card.publishers
    card.collection = res.collection
    card.date_publication = res.date_publication
    card.thickness = res.thickness
    card.height = res.height
    card.width = res.width
    card.weight = res.weight
    card.nb_pages = res.nb_pages
    card.presedit = res.presedit
    card.presmag = res.presmag

    # XXX: check if needed and save. These are not DB-critical data either.
    # card.save()

    # We were returning "res" from the cache and this lead to cache errors (shelf, last_soldcard_date…).
    # Returning the fresh card is much better.
    return card, messages

@cached(cache=TTLCache(maxsize=1024, ttl=72000))  # 20 hours
def _call_update_from_dilicom_with_cache(card):
    return update_from_dilicom(card)

def update_from_dilicom(card):
    """
    Update data: price, price excl vat, vat, availability, distributor,
    dimensions, weight.

    - card: card object.

    Return: a tuple
    - card,
    - list of messages (str).
    """
    if not card or not card.isbn or not (is_isbn(card.isbn)):
        return card, []

    scraper = getattr(globals()['dilicom'], "Scraper")
    res, traces = scraper(card.isbn).search()

    # XX: try to use first Card.update_from_dict.
    if res and res[0]:
        to_save = False
        res = res[0]
        if card.price != res['price']:
            card.price = res['price']
            to_save = True

        if res['theme'] and card.theme != res['theme']:
            card.theme = res['theme']
            to_save = True

        if res.get('width') and card.width != res.get('width'):
            card.width = res.get('width')
            to_save = True
        if res.get('weight') and card.weight != res.get('weight'):
            card.weight = res.get('weight')
            to_save = True
        if res.get('height') and card.height != res.get('height'):
            card.height = res.get('height')
            to_save = True
        if res.get('thickness') and card.thickness != res.get('thickness'):
            card.thickness = res.get('thickness')
            to_save = True
        if res.get('details_url') and card.width != res.get('details_url'):
            card.details_url = res.get('details_url')
            to_save = True
        if res.get('data_source') and card.width != res.get('data_source'):
            card.data_source = res.get('data_source')
            to_save = True

        if res.get('distributor_gln'):
            # XXX: see also Card.update_from_dict
            if not card.distributor or (card.distributor and card.distributor.gln != res.get('distributor_gln')):
                dist, created = get_distributor_from_gln(res.get('distributor_gln'))
                if dist:
                    card.distributor = dist
                    to_save = True

        # # "price_excl_vat" is already a model property.
        # # We'll use the "fmt" one.
        # if card.price_excl_vat != res.get('price_excl_vat'):
        #     card.price_excl_vat = res.get('price_excl_vat')
        #     to_save = True

        if to_save:
            card.save()

        # trick: add object fields, not saved to the DB, only for display.
        card.vat1 = res.get('vat1')
        card.price_excl_vat_fmt = res.get('price_excl_vat_fmt')
        card.dilicom_price_excl_vat = res.get('dilicom_price_excl_vat')

        card.availability = res['availability']
        card.availability_fmt = res['availability_fmt']

    else:
        res = card
    return card, traces


@cached(cache=TTLCache(maxsize=1024, ttl=72000))  # 20 hours
def update_from_electre_with_cache(card):
    res = update_from_electre(card)
    # see Dilicom related function for info.
    res.shelf = card.shelf
    res.comment = card.comment
    res.summary = card.summary
    res.modified = card.modified
    return res

def update_from_electre(card):
    """
    Update this card from Electre's API (using the pyelectre Electre).
    Search by ISBN and update the important fields.
    """
    if not card or not card.isbn or not (is_isbn(card.isbn)):
        return card, []

    res, traces = pyelectre.search_on_electre(card.isbn)
    if res and res[0]:
        res = res[0]
        newcard = Card.update_from_dict(card, res,
                                        distributor_name=res.get('distributor_name'),
                                        distributor_gln=res.get('distributor_gln'),
            )
        return newcard

    return card, traces


def _check_csv_format(rows):
    """
    For all these rows, check we have an ISBN and an integer.

    Return: boolean, messages.
    """
    for row in rows:
        if not row and not row[0]:
            return None, ["rows missing data. They are expected to have an ISBN and a number."]
        if not is_isbn(row[0]):
            return False, ["Not all lines contain an ISBN."]
        try:
            int(row[1])
        except Exception as e:
            print(e)
            return False, ["Not all lines contain a valid number."]

    return True, []

def extract_all_isbns_quantities(inputrows):
    """
    From a list of lines, find the ISBNs and an associated quantity.
    Ignore the other columns.

    If we have only lines of ISBNs, OK, associate 1 quantity by default.
    If we have a comma-separated CSV with an ISBN and a number, OK.
    If we have a space-separated CSV with an ISBN and a number, OK.

    If we have a CSV with more rows… find the quantity. Not implemented.

    Ignore other optional columns.

    Ignore the first line(s) starting by // (form embedded comment).

    Return: a tuple:
    - list of tuples ISBN/quantity,
    - list of messages (strings)
    """
    msgs = utils.Messages()
    lines = []  # inputrows, cleaned up.
    rows = []   # final list of tuples.
    res = []

    def falsy_col(col):
        return col in [None, '', u'']

    def is_comment(line):
        return line.startswith('//')

    def contains_spacing(line):
        """
        - space or tab \t
        """
        return ' ' in line or '\t' in line

    def find_space_in_line(line):
        if ' ' in line:
            return ' '
        if '\t' in line:
            return '\t'

    # Quick cleanup.
    for row in inputrows:
        if row and not is_comment(row) and not row in [u'']:
            lines.append(row.strip())

    # ;-separated CSV?
    if lines and ';' in lines[0]:
        for row in lines:
            isbn, quantity = row.split(';')
            if isbn and not falsy_col(isbn):
                try:
                    quantity = int(quantity)
                    if not quantity:
                        quantity = 1  # just in case
                    rows.append([isbn.strip(), quantity])
                except Exception as e:
                    # logging.warning("extract_all_isbns_quantities: could not parse quantity {} to int: {}".format(quantity, e))
                    msgs.add_warning("Could not parse quantity for ISBN {}".format(isbn))

    # We have a space-separated ISBN_qty
    elif lines and contains_spacing(lines[0]):
        thespace = find_space_in_line(lines[0])
        if thespace and is_isbn(lines[0].split(thespace)[0]):
            for row in lines:
                isbn, quantity = re.split(thespace, row, maxsplit=1)
                if isbn and not falsy_col(isbn):
                    try:
                        quantity = int(quantity.strip())
                        if not quantity:
                            quantity = 1
                        rows.append([isbn.strip(), quantity])
                    except Exception as e:
                        msgs.add_warning("Could not parse quantity for ISBN {}".format(isbn))

        else:
            msgs.add_warning("Could not parse the line {}".format(row))

    # We simply have a list of ISBNs:
    elif lines and is_isbn(lines[0]):
        for line in lines:
            rows.append([line, 1])  # default quantity
        return rows, []
    else:
        print("extract_all_isbns_quantities: dead end.")
        msgs.add_error("Could not parse input. Bad lines format.")

    # Collect ISBNs (and numbers).
    if rows and rows[-1] and len(rows[-1]) > 1:
        # Find where is the ISBNs (should be usually 1st position).
        first_col = rows[0][0]
        second_col = rows[0][1]
        try:
            int(second_col)
            is_number = True
        except Exception as e:
            print(e)
            is_number = False
        if is_isbn(first_col) and is_number:
            # Check all first columns are ISBNs, all second are numbers
            valid_csv, messages = _check_csv_format(rows)
            if not valid_csv:
                return None, messages
            res = rows

    else:
        msgs.add_warning("We couldn't recognize data. Please check the first rows are valid ISBNs and numbers. They can be CSV rows separated by ';' or only an ISBN on each line.")
        return [], msgs.msgs

    return res, msgs.msgs

def bulk_import_from_dilicom(isbns):
    """
    Import this list of ISBNs by batches of 100.

    Only Dilicom provides this batch request for now.
    Its search() methods handles the batch requests.

    - isbns: list of strings, all verified to be valid isbns.

    Return: a tuple results, list of messages (strings).
    """
    if not dilicom_enabled():
        return [], ["Dilicom is not enabled."]
    fel = dilicom.Scraper(*isbns)
    dicts, messages = fel.search()
    return dicts, messages

class Echo(object):
    """
    An object that implements just the write method of the file-like
    interface.
    """
    # taken from Django docs
    def write(self, value):
        """
        Write the value by returning it, instead of storing in a buffer.
        """
        return value


def cards2csv(cards, with_sells=False, date_sell_start_string="", date_sell_end_string=""):
    """
    Cards: as dicts.

    - with_sells: add a column "sells_period_quantity" (counting the sells between two dates).

    Return: CSV (string).
    """
    pseudo_buffer = Echo()
    writer = unicodecsv.writer(pseudo_buffer, delimiter=b';')
    content = writer.writerow(b"")

    def create_row(card):
        price = it['price']
        discount = it.get('distributor_discount')
        row = [
            it['created'],
            it['isbn'],
            it['title'],
            it['authors_repr'],
            price,
            it['quantity'],
            it['shelf'],
            it.get('distributor_repr'),
            discount,
            utils.price_excl_vat(price, card.get('vat', 5.5) or 5.5),
            utils.roundfloat(utils.price_excl_vat_discounted(price, discount, vat=card.get('vat', 5.5) or 5.5), rounding="ROUND_HALF_EVEN"),
            utils.roundfloat(utils.price_minus_percent(price, discount), rounding="ROUND_HALF_EVEN"),
        ]
        if with_sells:
            quantity = it.get('sells_period_quantity')
            if quantity is None:
                pass
            row.append(quantity)

        return row

    rows = [create_row(it) for it in cards]

    header = [_("created at"),
              _("isbn"),
              _("title"),
              _("authors"),
              _("price"),
              _("quantity"),
              _("shelf"),
              _("distributor"),
              _("discount"),
              _("prix HT"),
              _("prix HT remisé"),
              _("prix TTC remisé"),
    ]
    if with_sells:
        header_name = "{} {}--{}".format(
            _("sells"),
            date_sell_start_string,
            date_sell_end_string,
        )
        header.append(header_name)
    rows.insert(0, header)
    content = b"".join([writer.writerow(row) for row in rows])
    return content


def format_price_for_locale(price):
    """
    For french-ish locales, a comma should be the decimal separator.
    """
    try:
        if settings.LOCALE_FOR_EXPORTS == "fr":
            return str(price).replace('.', ',')
    except:  # noqa: E722
        pass
    try:
        return str(price)
    except:  # noqa: E722
        return price


def is_htmx_request(meta_dict):
    """
    Is this request fired by HTMX? It should have HTTP_HX_REQUEST or HX_REQUEST
    in its headers.

    meta_dict: the request.META dictionnary.

    Return: bool
    """
    # dev reminder: see https://htmx.org/attributes/hx-select/
    # to have one big template and choose the tree we want to "swap".
    return meta_dict.get('HTTP_HX_REQUEST') or meta_dict.get('HX_REQUEST')

def to_short_date(date):
    if not date:
        return
    TWO_DIGITS_SPEC = '0>2'
    res = "{}-{}-{}".format(date.year,
                             format(date.month, TWO_DIGITS_SPEC),
                             format(date.day, TWO_DIGITS_SPEC))
    return res
