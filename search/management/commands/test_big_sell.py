#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import datetime

from django.utils import timezone
import unicodecsv
from django.core.management.base import BaseCommand

from search.models import Card, Sell
# from search.models.common import get_payment_abbr
# from search.models.utils import is_book
# from search.views_utils import Echo

# py2/3
try:
    input = raw_input
except NameError:
    pass

"""
Before any speed up: selling 100 cards took 106s.


After avoiding many Card.save(): 23s
=> x4 speed up, but not enough.

After removing access to place_obj and place.save: 19.5s
=> x5 speed up.

After atomic transaction => 13s, nearly x2, total x8
(atomic in Card.sell, not in Sell.sell_cards for all Card.sell calls)

Atomic transaction for ALL card.save (around the sell) => 3s. Just ridiculous.

Spy on the process and get a flamegraph with py-spy:

sudo env "PATH=$PATH" py-spy record --duration 10 -p <Python PID HERE>  --output pyspy-profile.svg --nonblocking --rate 100

and start the Python process.
"""

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--ids',
                            dest="ids",
                            action="store",
                            help="card ids, followed by their selling prices, followed by their quantities, all separated by comas.")

    def run(self, *args, **options):
        """
        """
        DATE_FORMAT = "%Y-%m-%d"
        # 100 cards:
        cards = Card.objects.all()[:100]
        # only 10:
        # cards = Card.objects.all()[:10]
        to_sell = []
        for card in cards:
            to_sell.append({
                "id": card.pk,
                "price_sold": card.price,
                "quantity": 1,
            })


        start = timezone.now()
        print("*************************************************************************")
        print("***** Caution please! This script is run agaist the primary DB, not a test DB.")
        print("***** ensure you are NOT running this in production.")
        print("*************************************************************************")
        import ipdb; ipdb.set_trace()
        sell, status, messages = Sell.sell_cards(to_sell)
        end = timezone.now()
        print("--- big sell took: {}".format(end - start))

    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
