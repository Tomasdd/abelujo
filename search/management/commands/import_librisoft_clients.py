#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Import an initial CSV client file with:

- name
- address
- email
- phone
- date created TODO:
- …


Usage:

./manage.py import_librisoft_clients -i path/to.csv

"""
from __future__ import print_function
from __future__ import unicode_literals

import re
import datetime
# import dateparser

from django.core.management.base import BaseCommand

# from search.datasources.bookshops.frFR.dilicom import dilicomScraper
from search.models import Client
from search.models import Distributor
# from search.models.api import to_int

COL_FIRSTNAME = "TITRE,C,20"
COL_NAME = "NOM1,C,35"

def find_separator(line, default=";"):
    if ";" in line:
        return ";"
    elif "," in line:
        return ","
    return default

def intFromText(text):
    """
    Extract the integer from text with a regexp.
    """
    if not text:
        return
    match = re.search('\d+[,\.]?\d*', text)
    if match:
        res = match.group()
        return res

class Command(BaseCommand):

    help = "Import a csv file with many columns."
    cards_created = []

    def add_arguments(self, parser):
        parser.add_argument(
            '-i',
            dest='input',
            help='CSV file of input.',
        )
        parser.add_argument(
            '-e',
            dest='encoding',
            help='Encoding of the file. Defaults to UTF8. Might happen to be latin.',
        )

    def _decode(self, txt):
        if txt and self.encoding:
            return txt.decode(self.encoding)
        else:
            return txt

    def add_row(self, row):
        """
        Save this row as an object in our DB.

        - row: csv DictReader next() object.

        """
        # is_client = row.get("client")
        # is_client = is_client in ['1', ]
        is_client = True

        def get_col(col):
            return self._decode(row.get(col)) or ""

        def is_valid(txt):
            if txt and txt not in ['', ""]:  # unicode…
                return True
            return False

        firstname = get_col(COL_FIRSTNAME)
        name = get_col(COL_NAME)
        # also NOM3,C,35
        code_client = get_col('CODE')
        code_fournisseur = get_col('code_fournisseur')
        address1 = get_col('RUE,C,35')
        zip_code = get_col('CODEPOST,C,10')
        city = get_col('COMPLEMENT,C,35')
        # country = get_col('country')
        # country_code = get_col('country_code')
        telephone = get_col('TELEPHONE,C,16')
        # website = get_col('website')
        email = get_col('EMAIL,C,100')
        created = get_col('DATECRE,C,8')

        DATE_FORMAT = "%Y%m%d"
        if created:
            created = datetime.datetime.strptime(created, DATE_FORMAT)

        # discount = get_col('discount')
        # if discount and is_valid(discount):
        #     try:
        #         discount = float(discount)
        #     except Exception as e:
        #         print("Warn: could not parse discount of client {}: {}".format(name, e))
        #         discount = 0
        # else:
        #     discount = 0

        comment = get_col('NOM3,C,35')
        if code_client and is_valid(code_client):
            comment += code_client
        if code_fournisseur and is_valid(code_fournisseur):
            comment += code_fournisseur

        try:
            if is_client:
                # Get unique client by name and firstname.
                client, obj_created = Client.objects.get_or_create(
                    name=name,
                    firstname=firstname,
                )

                # Save or update other fields.
                for tup in [("email", email),
                            ("telephone", telephone),
                            ("adresse1", address1),
                            ("city", city),
                            ("zip_code", zip_code),
                            ("comment", comment),
                            ("created", created),
                            ]:
                    # client.telephone = telephone
                    if tup[1]:
                        # lol, I wanted to write Lisp here:
                        # just give me quote and unquote.
                        # setattr(client, 'key, ,key)
                        setattr(client, tup[0], tup[1])

                client.save()

                if obj_created:
                    print("+++ new client: {}".format(client))
                else:
                    print("->   existing client: {}".format(client))
        except Exception as e:
            print("script error creating new client: {}".format(e))


    def run(self, *args, **options):
        """
        Import clients from a Dilicom exported file, .dbf turned to .csv.
        """
        csvfile = options.get('input')
        if not csvfile.endswith('csv'):
            self.stdout.write("Please give a .csv file as argument.")
            exit(1)

        # Useful when the CSV file is not UTF8.
        self.encoding = options.get('encoding')

        import csv
        from pprint import pprint
        with open(csvfile, "r") as f:
            mycsv = csv.DictReader(
                f,
                delimiter=b';',
            )

            for row in mycsv:
                print("CSV line {}".format(mycsv.line_num))
                pprint(row)
                try:
                    self.add_row(row)
                except Exception as e:
                    print("Script: unexpected error with add_row, line {}:".format(mycsv.line_num))
                    print(e)

        self.stdout.write("Done.")

    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
