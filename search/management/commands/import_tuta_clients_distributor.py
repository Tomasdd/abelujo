#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Import an initial CSV files with:

- client or distributor: 0 or 1
- name
- client: 0/1
- fournisseur: 0/1
- …


Usage:

./manage.py import_full_csv -i path/to.csv

"""
from __future__ import print_function
from __future__ import unicode_literals

import re
# import dateparser

from django.core.management.base import BaseCommand

# from search.datasources.bookshops.frFR.dilicom import dilicomScraper
from search.models import Client
from search.models import Distributor
# from search.models.api import to_int

# py2/3
try:
    input = raw_input
except NameError:
    pass

def find_separator(line, default=None):
    if ";" in line:
        return ";"
    if "," in line:
        return ","
    return default

def intFromText(text):
    """
    Extract the integer from text with regexp.
    """
    if not text:
        return
    match = re.search('\d+[,\.]?\d*', text)
    if match:
        res = match.group()
        return res

class Command(BaseCommand):

    help = "Import a csv file with many columns."
    cards_created = []

    def add_arguments(self, parser):
        parser.add_argument(
            '-i',
            dest='input',
            help='CSV file of input.',
        )
        parser.add_argument(
            '-e',
            dest='encoding',
            help='Encoding of the file. Defaults to UTF8. Might happen to be latin.',
        )

    def _decode(self, txt):
        if txt and self.encoding:
            return txt.decode(self.encoding)
        else:
            return txt

    def add_row(self, row):
        """
        Save this row as an object in our DB.

        - row: csv DictReader next() object.

        """
        is_client = row.get('client')
        is_client = is_client in ['1', ]

        def get_col(col):
            return self._decode(row.get(col))

        def is_valid(txt):
            if txt and txt not in ['', ""]:  # unicode…
                return True
            return False

        name = get_col('name')
        code_client = get_col('code_client')
        code_fournisseur = get_col('code_fournisseur')
        adresse = get_col('adresse')
        postal_code = get_col('postal_code')
        city = get_col('city')
        country = get_col('country')
        # country_code = get_col('country_code')
        telephone = get_col('telephone')
        url = get_col('url')
        email = get_col('email')
        discount = get_col('discount')
        if discount and is_valid(discount):
            try:
                discount = float(discount)
            except Exception as e:
                print("Warn: could not parse discount of client {}: {}".format(name, e))
                discount = 0
        else:
            discount = 0
        comment = get_col('comment')
        if code_client and is_valid(code_client):
            comment += code_client
        if code_fournisseur and is_valid(code_fournisseur):
            comment += code_fournisseur

        try:
            if is_client:
                client, created = Client.objects.get_or_create(
                                name=name,
                                telephone=telephone,
                                email=email,
                                website=url,
                                address1=adresse,
                                zip_code=postal_code,
                                city=city,
                                country=country,
                                comment=comment,
                )
                client.save()
                print("+++ new client: {}".format(client))
        except Exception as e:
            print("script error creating new client: {}".format(e))
            # import ipdb; ipdb.set_trace()

        # If not a client, it's a distributor (specific CSV rule).
        try:
            if not is_client:
                dist, created = Distributor.objects.get_or_create(
                                   name=name,
                                   discount=discount,
                                   telephone=telephone,
                                   email=email,
                                   website=url,
                                   address1=adresse,
                                   postal_code=postal_code,
                                   city=city,
                                   country=country,
                                   comment=comment,
                                )
                dist.save()
                print("+++ new dist: {}".format(dist))
        except Exception as e:
            print("script error creating new distributor: {}".format(e))
            # import ipdb; ipdb.set_trace()

    def run(self, *args, **options):
        """
        Import cards and set their quantity.

        The script can add some cards and exit. If it is run a second time, it will process
        everything again.

        It is indempotent: we *set* the card's quantity, we don't *add* it to the stock.
        """
        csvfile = options.get('input')
        if not csvfile.endswith('csv'):
            self.stdout.write("Please give a .csv file as argument.")
            exit(1)

        # Useful when the CSV file is not UTF8.
        self.encoding = options.get('encoding')

        import csv
        from pprint import pprint
        with open(csvfile, "r") as f:
            mycsv = csv.DictReader(f)

            for row in mycsv:
                print("CSV line {}".format(mycsv.line_num))
                pprint(row)
                try:
                    self.add_row(row)
                except Exception as e:
                    print("Script: unexpected error with add_row, line {}:".format(mycsv.line_num))
                    print(e)
                    # import ipdb; ipdb.set_trace()

        self.stdout.write("Done.")

    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
