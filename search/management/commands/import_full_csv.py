#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Import an initial CSV files with many columns.

Adds quantities (doesn't set them).

Skip books with ISBN already in DB.
Skip lines with no ISBN and same title in DB.

Separator must be ";".

Arguments:

-e encoding: defaults to utf8, but it could be "latin".

Columns names:

- ISBN
- code NEW: if no ISBN, take "code".
- title
- price
- price_bought
- author (multiple authors comma-separated)
- vat
- quantity
- date_created
- year_published
- publisher
- distributor
- distributor_name NEW: in case there are two columns giving more or less the same data…
- lang
- ref
- height, width
- available
- shelf
- discount
- comment
- deposit
- …

Dev: we should validate all input, all the columns' data before processing.

Usage:

./manage.py import_full_csv -i path/to.csv

"""

#
# XXX: speed up DB access by getting ALL data in memory beforehand.
# And better run the script on a machine with no HDD.
#


from __future__ import print_function
from __future__ import unicode_literals   # delimiter must be a string, not unicode…

import io
# import csv  # don't: unicode issues
import unicodecsv as csv
import re
from pprint import pprint
import dateparser

from django.core.management.base import BaseCommand

from abelujo import settings
# from search.datasources.bookshops.frFR.dilicom import dilicomScraper
from search.models import Author
from search.models import Card
from search.models import CardType
from search.models import Distributor
from search.models import Preferences
from search.models import Publisher
from search.models import Shelf

from search.models import utils
# from search.models.api import to_int

# py2/3
try:
    input = raw_input
except NameError:
    pass

def find_separator(line, default=None):
    # XXX: not very good, better set the separator (delimiter) in code.
    # if '\t' in line:
        # return '\t'.encode('ascii')
    if ";" in line:
        return ";".encode('ascii')
    if "," in line:
        return ",".encode('ascii')
    return default

def extractNumberString(text):
    """
    Extract the number from text with a regexp.

    Return: string, representing a number.
    """
    if not text:
        return
    match = re.search('\d+[,\.]?\d*', text)
    if match:
        res = match.group()
        return res

class Command(BaseCommand):

    help = "Import a csv file with many columns."
    cards_created = []

    def add_arguments(self, parser):
        parser.add_argument(
            '-i',
            dest='input',
            help='CSV file of input.',
        )
        parser.add_argument(
            '-e',
            dest='encoding',
            help='Encoding of the file. Defaults to UTF8. Might happen to be latin.',
        )

    def _decode(self, txt):
        if txt and self.encoding:
            return txt.decode(self.encoding)
        else:
            return txt

    def add_row(self, row):
        """
        Save this row as a Card object in our DB.

        - row: csv DictReader next() object.

        Return:
        """
        def get_col(col):
            col = row.get(col)
            if col:
                col = col.strip()
            return col

        def is_valid(txt):
            if txt and txt not in ['', ""]:  # unicode…
                return True
            return False

        def parse_float(txt):
            txt = txt.replace(",", ".")
            try:
                return float(txt)
            except Exception as e:
                print("Error parsing float from {}: {}".format(txt, e))
                import ipdb; ipdb.set_trace()

        title = get_col('title')
        if not title:
            print("skipping line with no title")
            return

        print("Nouveau titre: {}".format(title))

        comment = get_col('comment') or ""
        lang = get_col('lang')
        height = get_col('height')
        ref = get_col('ref')

        if height and is_valid(height):
            height = parse_float(height)
            # we want an int… (height in mm)
            height = int(height * 100)
        else:
            height = None

        width = get_col('width')
        if width and is_valid(width):
            width = parse_float(width)
            width = int(width * 100)
        else:
            width = None

        price = get_col('price')
        # In the price column, we could have
        # - a float: 9.99, with a dot or a comma
        # - a string: "€9,99"
        # - and maybe a string with a price without comma,
        # maybe after working on the csv source: "€999"
        if price and is_valid(price):
            price = parse_float(price)
            # price = extractNumberString(price)
            if price:
                try:
                    price = float(price)
                    # price = price / 100.0
                except Exception as e:
                    print("Error extracting price: {}".format(e))
                    print(title)
                    price = 0.0
        else:
            price = None

        vat = get_col('vat')
        if vat and is_valid(vat):
            vat = parse_float(vat)
        else:
            # set default below with card_type.
            pass

        col = get_col('ISBN')
        isbn = ""
        existing = None
        card = None
        if not col:
            # If no ISBN, get the "code" column.
            col = get_col('code')
        # Here we accept any string as the ISBN: it can be a client special code.
        # The real ISBN check is made later in the application,
        # when we send commands for example.
        # if col and len(col) in [10, 11, 12, 13]:
        if col:
            isbn = col
            existing = Card.objects.filter(isbn=isbn).first()
        else:
            existing = Card.objects.filter(title=title).first()
            if existing:
                print("Found a similar title alreayd (without ISBN):")
                print(title)

        if existing:
            print("Existing card found in DB. Do nothing.")
            print(title)
            return

        if not existing:
            card = Card(title=title,
                        isbn=isbn,
                        in_stock=True,
                        height=height,
                        width=width,
                        price=price,
                        comment=comment,
                        lang=lang,
                        )

        if vat:
            card.vat = vat
        try:
            card.save()
        except Exception as e:
            print("Script error with the first card.save()")
            print(e)
            import ipdb; ipdb.set_trace()

        self.cards_created.append(card)

        print("+++ new card: {}".format(card))

        # Card Type
        if ref:
            if ref.startswith("MUS"):
                card.card_type = self.type_cd
            elif ref.startswith("FILM"):
                card.card_type = self.type_dvd
            elif ref.startswith("PROD"):
                card.card_type = self.type_other
            elif ref.startswith("REV"):
                card.card_type = self.type_newspaper
            elif ref.startswith("OUV"):
                card.card_type = self.type_book
                card.vat = 5.5  # set default VAT
            elif ref.startswith("LCD"):
                card.card_type = self.type_lcd
            else:
                card.card_type = self.type_unknown
                # card.card_type = self.type_book

        try:
            card.save()
        except Exception as e:
            print("script error with card type: {}".format(e))

        # Quantity
        quantity = row.get('quantity')
        if quantity:
            try:
                quantity = int(quantity)
            except Exception as e:
                quantity = 0
        else:
            quantity = 0

        # Add in default place.
        try:
            if quantity:
                self.default_place.add_copy(card, nb=quantity)
            elif quantity is not None:
                card.quantity = 0
        except Exception as e:
            print("Script error with quantity:")
            print(e)

        nb_pages = row.get('nb_pages')
        nb_pages = extractNumberString(nb_pages)
        if nb_pages:
            card.nb_pages = nb_pages

        price_bought = row.get('price_bought')
        if price_bought and card.price and price_bought != row.get('price'):
            card.price_bought = parse_float(price_bought)

        nb_titres = row.get('nb_titres')
        if nb_titres:
            nb_titres = self._decode(nb_titres)
            try:
                card.comment += nb_titres
                card.save()
            except Exception as e:
                print("Script error with nb_titres:")
                print(e)

        # update:
        # date_created, author(s), shelf, is_unavailable
        date_created = row.get('date_created')
        if date_created:
            card.created = dateparser.parse(date_created)

        #
        # Shelf and deposit.
        #
        INCLUDE_DEPOSIT_NAMES = []
        try:
            INCLUDE_DEPOSIT_NAMES = settings.INCLUDE_DEPOSIT_NAMES
        except Exception as e:
            import ipdb; ipdb.set_trace()
            pass

        def include_deposit(name):
            """
            If no filter list is given, take all deposits in the "deposit" column.
            But for some clients, we want to filter the deposits.
            """
            if not INCLUDE_DEPOSIT_NAMES:
                return True
            s = name.lower()
            res = s in INCLUDE_DEPOSIT_NAMES
            if res:
                print("---- include this deposit: ", s)
            return res

        try:
            shelf_name = get_col('shelf')
            shelf = None
            is_deposit = get_col('deposit')
            if is_deposit and include_deposit(is_deposit):
                shelf, created = Shelf.objects.get_or_create(name=is_deposit, is_deposit=True)
            elif shelf_name:
                shelf, created = Shelf.objects.get_or_create(name=shelf_name)
            if shelf:
                card.shelf = shelf
        except Exception as e:
            print("script error with shelf and/or deposit: {}".format(e))
            return

        name = get_col('author')
        authors = []
        if name and is_valid(name):
            # split multiple authors by ","
            if "," in name or ',' in name:
                print("multiple authors!")
                authors = name.split(",")
            else:
                authors = [name]

            try:
                author, created = Author.objects.get_or_create(name=name)
                if author:
                    card.authors.add(author)
                    card.save()
            except Exception as e:
                print("Script error with author:")
                print(e)
                return

        is_unavailable = get_col('available')
        try:
            # is_unavailable = self._decode(is_unavailable)
            is_unavailable = is_unavailable in ['epuise', "épuisé"]
            if is_unavailable:
                card.is_unavailable = True
        except Exception as e:
            print("Script error with is_unavailable:")
            print(e)
            return

        publisher = get_col('publisher')
        try:
            if publisher:
                publisher = self._decode(publisher)
                pub, created = Publisher.objects.get_or_create(name=publisher)
                if pub:
                    card.publishers.add(pub)
        except Exception as e:
            print("Script error with publisher:")
            print(e)
            return

        year_published = get_col('year_published')
        if year_published:
            try:
                # XXX: use date_publication
                year_published = dateparser.parse(year_published)
                card.year_published = year_published
            except Exception as e:
                print("Script error with year_published:")
                print(e)
                return

        fournisseur = get_col('distributor')
        if not fournisseur:
            # Try another column:
            fournisseur = get_col('distributor_name')
        try:
            if fournisseur:
                distributor = Distributor.objects.filter(name=fournisseur).first()
                if not distributor:
                    # yes, do both queries, we got a double distributor.
                    distributor, created = Distributor.objects.get_or_create(name=fournisseur)
                discount = get_col('discount')
                if discount and is_valid(discount):
                    try:
                        discount = parse_float(discount)
                    except Exception as e:
                        print("could not parse discount: {}".format(e))
                        print(title)
                        discount = 0
                else:
                    discount = 0

                distributor.discount = discount
                distributor.save()
                card.distributor = distributor
                # card.save()
        except Exception as e:
            print("Script error with fournisseur:")
            print(e)
            return

        try:
            card.save()
        except Exception as e:
            print("Script error with last card.save():")
            print(e)

        return card

    def run(self, *args, **options):
        """
        Import cards and set their quantity.

        The script can add some cards and exit. If it is run a second time, it will process
        everything again.

        It is indempotent: we *set* the card's quantity, we don't *add* it to the stock.
        """
        WARN_MSG = "***** This script sets the quantities, it doesn't add them… or does it? LOL *****"   # TODO:
        print(WARN_MSG)
        yes = input("Continue ? [Y/n]")
        if yes not in ["", "Y"]:
            print("quit.")
            exit(1)

        csvfile = options.get('input')
        if not csvfile.endswith('csv'):
            self.stdout.write("Please give a .csv file as argument.")
            exit(1)

        # Useful when the CSV file is not UTF8.
        self.encoding = options.get('encoding')

        self.default_place = Preferences.prefs().default_place
        if not self.default_place:
            print("We couldn't find a default place. Please check and try again.")
            exit(1)

        delimiter = ';'.encode('ascii')
        with open(csvfile, "r") as f:
            mycsv = csv.DictReader(
                f,
                # delimiter=find_separator(f.readline()),
                delimiter=delimiter,
            )

            for row in mycsv:
                # if mycsv.line_num < 69:
                    # continue

                print("CSV line {}".format(mycsv.line_num))
                # XXX: debug:
                pprint(row)

                # get the different card types
                self.type_book = CardType.objects.filter(name="book").first()
                self.type_lcd, created = CardType.objects.get_or_create(name="LCD")
                self.type_cd = CardType.objects.filter(name="cd").first()
                self.type_dvd = CardType.objects.filter(name="dvd").first()
                self.type_other = CardType.objects.filter(name="other").first()
                self.type_newspaper = CardType.objects.filter(name="newspaper").first()
                self.type_unknown = CardType.objects.filter(name="unknown").first()

                # Deposit
                # self.deposit, _ = Deposit.objects.get_or_create(name="depot initial")

                # Create and add the line to DB.
                try:
                    self.add_row(row)
                except KeyboardInterrupt:
                    self.stdout.write("Aborting.")
                    exit(1)
                except Exception as e:
                    print("Script: unexpected error with add_row, line {}:".format(mycsv.line_num))
                    print(e)
                    # import ipdb; ipdb.set_trace()

        self.stdout.write("Cards created: {}".format(len(self.cards_created)))
        self.stdout.write("Done.")

    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
