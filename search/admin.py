# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.
from django.contrib import admin

from search.models import Author
from search.models import Basket
from search.models import Bill
from search.models.users import Bookshop
from search.models import Card
from search.models import CardType
from search.models import CouponGeneric
from search.models import Coupon
from search.models import Client
from search.models import Command
from search.models import Shelf
from search.models import Distributor
from search.models import Inventory
from search.models import InstitutionBasket
from search.models import Place
from search.models import Publisher
from search.models import Reservation
from search.models import Sell
from search.models import Tag

#########################################
# Actions
#########################################

from django.http import HttpResponse
import datetime
import unicodecsv


def export_to_csv(modeladmin, request, queryset):
    """
    Export selected clients to CSV.
    """
    def is_valid_field(field):
        if field.many_to_many or field.one_to_many:
            return False
        if field.name.endswith("_ascii"):
            return False
        return True

    def is_not_excluded(field):
        if field.name in modeladmin.exclude:
            return False
        return True

    opts = modeladmin.model._meta
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;' 'filename={}.csv'.format(opts.verbose_name)
    writer = unicodecsv.writer(response)
    fields = [field for field in opts.get_fields() if is_valid_field(field)]
    fields = [field for field in fields if is_not_excluded(field)]
    # Write a first row with header information
    writer.writerow([field.name for field in fields])
    # Write data rows
    for obj in queryset:
        data_row = []
        for field in fields:
            value = getattr(obj, field.name)
            if isinstance(value, datetime.datetime):
                value = value.strftime('%d/%m/%Y')
            data_row.append(value)
        writer.writerow(data_row)

    return response


export_to_csv.short_description = 'Export to CSV'  # short description


#########################################
# Admin pages
#########################################

# Custom admin for the client admin:
# show less things than the default admin,
# in particular no need to show the intermediate classes.
class MyAdmin(admin.AdminSite):
    site_header = 'Abelujo administration'
    site_title = 'Abelujo admin'

class CardAdmin(admin.ModelAdmin):
    class Meta:
        model = Card

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "shelf":
            # It seems we can't sort with the locale, because we'd return a list
            # and we must return a queryset. So lowercase names are not sorted properly.
            kwargs["queryset"] = Shelf.objects.order_by('name')

        return super(CardAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    search_fields = ["title", "authors__name", "isbn"]
    list_display = ("title", "distributor", "price",)
    list_editable = ("distributor", "price",)
    filter_horizontal = ("authors", "publishers",)
    exclude = ("in_stock", "title_ascii", "places")


class CardTypeAdmin(admin.ModelAdmin):
    class Meta:
        model = CardType


class AuthorAdmin(admin.ModelAdmin):
    class Meta:
        model = Author

    search_fields = ["name"]
    # list_editable = ("name",)

class BasketAdmin(admin.ModelAdmin):
    class Meta:
        model = Basket

    search_fields = ["name"]
    list_display = ["pk", "name", "comment", "archived", "is_box"]
    list_editable = ("name",)

class ClientAdmin(admin.ModelAdmin):
    class Meta:
        model = Client

    actions = [export_to_csv]
    search_fields = ["name", "firstname", "mobilephone", "address1", "city", "country", "zip_code"]
    list_display = ["name", "firstname", "mobilephone", "email", "is_institution", "discount"]
    # sauvegarder les changements ne marche pas :/
    # list_editable = ["name", "firstname", "mobilephone", "email", "discount"]
    list_filter = ["is_institution"]
    ordering = ["name", "firstname", "is_institution"]
    exclude = ("sms_history",)


class CommandAdmin(admin.ModelAdmin):
    class Meta:
        model = Command

    search_fields = ["supplier_name"]
    list_display = ("__unicode__", "supplier_name", "date_received", "date_paid")


class DistributorAdmin(admin.ModelAdmin):
    class Meta:
        model = Distributor

    search_fields = ["name", "gln", "city", "country", "postal_code"]
    list_display = ("name", "discount", "client_number", "email")
    list_editable = ("discount", "client_number", "email")
    ordering = ("name",)

class BillAdmin(admin.ModelAdmin):
    class Meta:
        model = Bill

    list_display = ("name", "ref", "total")

class InventoryAdmin(admin.ModelAdmin):
    class Meta:
        model = Inventory

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "shelf":
            kwargs["queryset"] = Shelf.objects.order_by('name')
            return super(InventoryAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    list_display = ("name", "created", "closed")

class InstitutionAdmin(admin.ModelAdmin):
    class Meta:
        model = InstitutionBasket


class PublisherAdmin(admin.ModelAdmin):
    class Meta:
        model = Publisher

    list_display = ("name",)
    list_editable = ("name",)
    search_fields = ["name"]
    ordering = ("name",)


class ReservationAdmin(admin.ModelAdmin):
    class Meta:
        model = Reservation

    actions = [export_to_csv]
    search_fields = ["card__title"]
    list_display = ("pk", "client", "card", "created", "archived")
    exclude = ["payment_meta", "payment_session", "payment_intent"]

class ShelfAdmin(admin.ModelAdmin):
    class Meta:
        model = Shelf

    def in_shelf(shelf):
        return shelf.cards_qty

    list_display = ("name", "is_deposit", in_shelf)
    list_editable = ("name", "is_deposit")
    search_fields = ["name"]
    ordering = ("name",)


admin.site.register(Author, AuthorAdmin)
admin.site.register(Basket, BasketAdmin)
admin.site.register(Bookshop)
admin.site.register(Bill, BillAdmin)
admin.site.register(Card, CardAdmin)
admin.site.register(CardType, CardTypeAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(CouponGeneric)
admin.site.register(Coupon)
admin.site.register(Command, CommandAdmin)
admin.site.register(Shelf, ShelfAdmin)
admin.site.register(Distributor, DistributorAdmin)
admin.site.register(Inventory, InventoryAdmin)
admin.site.register(InstitutionBasket, InstitutionAdmin)
admin.site.register(Place)
admin.site.register(Publisher, PublisherAdmin)
admin.site.register(Reservation, ReservationAdmin)
admin.site.register(Sell)
admin.site.register(Tag)

admin.site.add_action(export_to_csv)

admin_site = MyAdmin(name='myadmin')
admin_site.register(Author, AuthorAdmin)
admin_site.register(Basket, BasketAdmin)
admin_site.register(Card, CardAdmin)
admin_site.register(CardType, CardTypeAdmin)
admin_site.register(Client, ClientAdmin)
admin_site.register(Command, CommandAdmin)
admin_site.register(Distributor, DistributorAdmin)
admin_site.register(Place)
admin_site.register(Publisher, PublisherAdmin)
admin_site.register(Sell)
admin_site.register(Reservation, ReservationAdmin)
admin_site.register(Shelf, ShelfAdmin)
admin_site.register(Tag)

admin_site.add_action(export_to_csv)
