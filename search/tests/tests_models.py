#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Test the models.

Note: to compare two objects, don't use assertEqual but the == operator.
"""
from __future__ import unicode_literals

import datetime

import factory
import logging
from django.test import TestCase
from django.test import TransactionTestCase
from django.utils import timezone
from factory.django import DjangoModelFactory

from search.models import ALERT_ERROR
from search.models import ALERT_SUCCESS
from search.models import ALERT_WARNING
from search.models import get_best_sells
from search.models import Author
from search.models import Basket
from search.models import ReturnBasket
from search.models import Card
from search.models import CardType
from search.models import Command
from search.models import Distributor
from search.models import Inventory
from search.models import InventoryCommand
from search.models import Place
from search.models import PlaceCopies
from search.models import Preferences
from search.models import Publisher
from search.models import Reception
from search.models import Sell
from search.models import Shelf
from search.models import SoldCards
from search.models import history
from search.models import Stats
from search.models.utils import get_logger
from search.models.utils import distributors_match

from search.models import common

log = get_logger()

class AuthorFactory(DjangoModelFactory):
    class Meta:
        model = Author

    name = factory.Sequence(lambda n: "author test %d" % n)

class SellsFactory(DjangoModelFactory):
    class Meta:
        model = Sell

    created = datetime.date.today()

class DistributorFactory(DjangoModelFactory):
    class Meta:
        model = Distributor
    name = factory.Sequence(lambda n: "distributor test %s" % n)
    discount = 35

class PlaceFactory(DjangoModelFactory):
    class Meta:
        model = Place
    name = factory.Sequence(lambda n: "place test %s" % n)
    is_stand = False
    can_sell = True

class PreferencesFactory(DjangoModelFactory):
    class Meta:
        model = Preferences
    default_place = PlaceFactory()
    vat_book = "5.5"

class PublisherFactory(DjangoModelFactory):
    class Meta:
        model = Publisher
    name = factory.Sequence(lambda n: "publisher test %s" % n)

# DEPRECATED. Deletion date: beginning of 2024.
class InventoryFactory(DjangoModelFactory):
    class Meta:
        model = Inventory


ISBN = "9782757837009"


class ShelfFactory(DjangoModelFactory):
    class Meta:
        model = Shelf

    name = factory.Sequence(lambda n: "shelf test id %s" % (n + 1))

class BasketFactory(DjangoModelFactory):
    class Meta:
        model = Basket

    name = factory.Sequence(lambda n: "basket test id %s" % (n + 1))
    distributor = None

class CardFactory(DjangoModelFactory):
    """
    Create a Card.

    Price: 9.99,
    ISBN: 978 -> the save() method will set the type to "book".
    """
    class Meta:
        model = Card

    title = factory.Sequence(lambda n: 'card title é and ñ %d' % n)
    isbn = factory.Sequence(lambda n: "%d" % n)
    card_type = None
    # distributor = factory.SubFactory(DistributorFactory)
    distributor = None
    price = 9.99
    isbn = ISBN

class TestAuthors(TestCase):
    def setUp(self):
        self.author = AuthorFactory()
        self.author2 = AuthorFactory()

    def tearDown(self):
        pass

    def test_default(self):
        res = Author.search('1')
        self.assertEqual(1, len(res))

class TestCards(TestCase):
    def setUp(self):
        # Create card types
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        # a Publisher
        self.pub_name = "pub test"
        self.publisher = Publisher(name=self.pub_name)
        self.publisher.save()
        # create an author
        self.GOLDMAN = "Emma Goldman"
        self.goldman = Author(name=self.GOLDMAN)
        self.goldman.save()
        # create a Card
        self.fixture_isbn = ISBN
        self.fixture_title = "living my life"
        self.autobio = Card(title=self.fixture_title,
                            isbn=self.fixture_isbn,
                            shelf=ShelfFactory(),
                            card_type=typ)
        self.autobio.save()
        self.autobio.authors.add(self.goldman)
        self.autobio.publishers.add(self.publisher)
        # mandatory: unknown card type
        typ = CardType(name="unknown")
        typ.save()
        # a needed place:
        self.place_name = "test place"
        self.place = Place(name=self.place_name, is_stand=False, can_sell=True)
        self.place.save()
        self.place.add_copy(self.autobio, nb=1)
        # mandatory: preferences table
        self.preferences = Preferences(default_place=self.place).save()

    def tearDown(self):
        log.setLevel(logging.DEBUG)

    def test_to_dict(self):
        res = self.autobio.to_dict()
        assert res
        self.assertTrue('€' in res['price_fmt'])

    def test_shelf_repr(self):
        self.assertNotEqual("", self.autobio.shelf_repr)
        self.autobio.shelf = None
        self.autobio.save()
        self.assertEqual("", self.autobio.shelf_repr)

    def test_add(self):
        found = Card.objects.get(title__icontains="living")
        self.assertTrue(found.authors.all()[0] == self.goldman)
        self.assertEqual(found, self.autobio)

    def test_from_dict(self):
        TITLE = "Foo bar"
        ZINN = "zinn"
        card, msgs = Card.from_dict({"title": TITLE,
                                       "authors": [self.GOLDMAN, ZINN],
                                       "isbn": "foobar",
                                       "publishers": ['new publisher'],
                                       "distributor": "new dist",
                                       "location": "here"})
        self.assertTrue(card)
        self.assertEqual(card.title, TITLE)
        self.assertEqual(len(Author.objects.all()), 2)
        names = [aut.name for aut in card.authors.all()]
        self.assertTrue(ZINN in names)
        self.assertTrue(self.GOLDMAN in names)
        # Check that the author was created
        self.assertTrue(Author.objects.get(name=ZINN))
        self.assertEqual("new publisher", card.publishers.first().name)

    def test_exists(self):
        """Card.exists unit test.
        """
        exists = Card.exists({'isbn': self.fixture_isbn})
        self.assertTrue(exists)
        doesnt_exist, msgs = Card.exists({"isbn": "whatever",
                                               "title": "a different title"})
        self.assertFalse(doesnt_exist)
        # The same title is not enough.
        same_title, msgs = Card.exists({"title": self.fixture_title})
        self.assertFalse(same_title)
        self.assertTrue(msgs)
        good_authors, msgs = Card.exists({"title": self.fixture_title,
                                          "authors": [self.GOLDMAN]})
        self.assertTrue(good_authors)
        good_publishers, msgs = Card.exists({"title": self.fixture_title,
                                             "authors": [self.GOLDMAN],
                                             "publishers": [self.pub_name]})
        self.assertTrue(good_publishers)
        bad_authors, msgs = Card.exists({"title": self.fixture_title,
                                         "authors": "bad author"})
        self.assertFalse(bad_authors)
        bad_publishers, msgs = Card.exists({"title": self.fixture_title,
                                            "authors": [self.GOLDMAN],
                                            "publishers": ["not a pub"]})
        self.assertFalse(bad_publishers)

    def test_exists_many(self):
        # Second card with same title, other authors
        self.card2 = Card(title=self.fixture_title,
                          isbn=self.fixture_isbn,
                          shelf=ShelfFactory())
        self.card2.save()
        author = AuthorFactory()
        self.card2.authors.add(author)
        self.card2.save()

        # we find our card when many have the same title.
        same_authors, msgs = Card.exists({"title": self.fixture_title,
                                           "authors": [author.name]})
        self.assertTrue(same_authors)
        # same title but different authors.
        other_authors, msgs = Card.exists({"title": self.fixture_title,
                                           "authors": [AuthorFactory()]})
        self.assertFalse(other_authors)
        # only the same title. Not enough to find a similar card.
        no_pubs_no_authors, msgs = Card.exists({"title": self.fixture_title, })
        self.assertFalse(no_pubs_no_authors)
        self.assertTrue(msgs)

    def test_cards_in_stock(self):
        res = Card.cards_in_stock()
        self.assertEqual(1, len(res))
        # cards only in baskets should not appear.
        basket = BasketFactory.create()
        card = CardFactory.create()
        basket.add_copy(card)
        res = Card.cards_in_stock()
        self.assertEqual(2, Card.objects.count())
        self.assertEqual(1, len(res))

    def test_is_in_stock(self):
        card_dict = self.autobio.to_list()
        # xxx: strange: card_dict['isbn'] is "" but to_list()
        # returns the right thing :S
        # Without this, this test passes in TestCards, but not in the
        # full test suite.
        card_dict['isbn'] = self.autobio.isbn
        card_dict['id'] = None
        other = {
            "title": "doesn't exist",
            "isbn": None,
        }
        cards = Card.is_in_stock([card_dict, other])
        self.assertTrue(cards)
        self.assertEqual(cards[0]['in_stock'], self.autobio.quantity)
        self.assertEqual(cards[0]['id'], self.autobio.id)
        self.assertFalse(cards[1]['in_stock'])

    def test_from_dict_no_authors(self):
        TITLE = "I am a CD without authors"
        to_add, msgs = Card.from_dict({"title": TITLE})
        self.assertTrue(to_add)

    def test_update(self):
        #TODO: test that we update fields of the card when we use
        #from_dict but the card alreay exists.
        pass

    def test_search(self):
        # Should search with way more cards.
        res, meta = Card.search(["gold"], card_type_id=1)
        self.assertEqual(1, len(res))

    def test_search_notype(self):
        res, meta = Card.search(["gold"], card_type_id=999)
        self.assertFalse(res)

    def test_search_alltypes(self):
        res, meta = Card.search(["gold"], card_type_id=0)
        self.assertTrue(res)

    def test_search_only_type(self):
        # Doesn't pass data validation.
        self.assertTrue(Card.search("", card_type_id=1))

    def test_search_key_words(self):
        res, meta = Card.search(["liv", "gold"])
        self.assertEqual(1, len(res))

    def test_search_card_isbn(self):
        res, meta = Card.search([ISBN])
        self.assertEqual(len(res), 1)

    def test_search_shelf(self):
        res, meta = Card.search(["gold"], shelf_id=1)
        self.assertEqual(len(res), 1)
        # Shelf doesn't exist:
        res, meta = Card.search(["gold"], shelf_id=2)
        self.assertEqual(len(res), 0)

    def test_first_cards(self):
        res = Card.first_cards(10)
        self.assertEqual(len(res), 1)
        self.assertTrue(isinstance(res[0], Card))
        res = Card.first_cards(10, to_list=True)
        self.assertTrue(isinstance(res[0], dict))

    def test_sell(self):
        Card.sell(id=self.autobio.id, quantity=2)
        self.assertEqual(Card.objects.get(id=self.autobio.id).quantity, -1)

    def test_get_from_id_list(self):
        cards_id = [1]
        res, msgs = Card.get_from_id_list(cards_id)
        self.assertTrue(res)
        self.assertEqual(res[0].title, self.fixture_title)

    def test_get_from_id_list_non_existent(self):
        log.setLevel(logging.WARNING)
        cards_id = [1, 2]
        res, msgs = Card.get_from_id_list(cards_id)
        self.assertTrue(res)
        self.assertTrue(msgs)
        self.assertEqual(msgs[0]["message"], "The card of id 2 doesn't exist.")

    def test_placecopies(self):
        pass


class TestDistributor(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book", vat=5.5)
        self.dist = DistributorFactory()
        self.to_command = BasketFactory()

    def tearDown(self):
        pass

    def test_set_distributor(self):
        basket = BasketFactory()
        # Put cards in basket.
        for _ in range(3):
            basket.add_copy(CardFactory())

        basket.distributor = DistributorFactory()
        basket.save()
        # Set the distributor of these cards.
        self.dist.set_distributor(basket.copies.all())

        self.assertEqual(basket.copies.last().distributor, self.dist)


class TestPublisher(TestCase):
    """Testing the addition of a publisher to a card.
    """

    def setUp(self):
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        # create a Card
        self.autobio = Card(title="Living my Life", isbn="987")
        self.autobio.save()
        # create a publisher
        self.publishers = Publisher(name="agone")
        self.publishers.save()

    def test_publisher_existing(self):
        pub = "agone"
        obj, msgs = Card.from_dict({"title": "living", "publishers": [pub]})
        all_pubs = obj.publishers.all()
        self.assertEqual(1, len(all_pubs))
        self.assertEqual(pub.lower(), all_pubs[0].name)

    def test_many_publishers(self):
        pub = ["agone", "maspero"]
        obj, msgs = Card.from_dict({"title": "living", "publishers": pub})
        all_pubs = obj.publishers.all()
        self.assertEqual(len(pub), len(all_pubs))
        self.assertEqual(pub[0].lower(), all_pubs[0].name)

    def test_publisher_non_existing(self):
        pub = "Foo"
        obj, msgs = Card.from_dict({"title": "living", "publishers": [pub]})
        self.assertEqual(pub.lower(), obj.publishers.all()[0].name.lower())
        publishers = Publisher.objects.all()
        self.assertEqual(2, len(publishers))


class TestPlace(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.place = PlaceFactory.create()
        self.card = CardFactory.create()
        # Preferences
        self.preferences = Preferences(default_place=self.place).save()

    def tearDown(self):
        pass

    def test_nominal(self):
        self.assertTrue("place" in self.place.__unicode__())
        self.assertEqual('/en/databasesearch/place/1/', self.place.get_absolute_url())

    def test_add_copy(self):
        self.assertEqual(0, history.Entry.objects.count())
        # normal
        self.assertFalse(self.card.in_stock)
        self.place.add_copy(self.card)
        self.assertTrue(self.card.in_stock)
        self.assertEqual(1, self.place.placecopies_set.count())
        self.assertEqual(1, self.place.placecopies_set.first().nb)
        self.assertEqual(1, history.Entry.objects.count())
        # give the quantity.
        self.place.add_copy(self.card, nb=2)
        self.assertEqual(3, self.place.placecopies_set.first().nb)
        self.assertEqual(2, history.Entry.objects.count())
        # do not add, set the quantity.
        self.place.add_copy(self.card, nb=2, add=False)
        self.assertEqual(2, self.place.placecopies_set.first().nb)
        self.assertEqual(3, history.Entry.objects.count())


class TestPlaceCopies(TestCase):

    def setUp(self):
        # Create a relation Card - PlaceCopies - Place
        self.place = Place(name="here", is_stand=False, can_sell=True)
        self.place.save()
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.card = Card(title="test card")
        self.card.save()
        self.nb_copies = 9
        self.pl_cop = PlaceCopies(card=self.card, place=self.place, nb=self.nb_copies).save()
        self.prefs = Preferences(default_place=self.place).save()

    def tearDown(self):
        pass

    def test_add_copies(self):
        self.place.add_copy(self.card)
        new_nb = self.place.placecopies_set.get(card=self.card).nb
        self.assertEqual(self.nb_copies + 1, new_nb)
        self.place.add_copy(self.card, 10)
        new_nb = self.place.placecopies_set.get(card=self.card).nb
        self.assertEqual(self.nb_copies + 1 + 10, new_nb)

    def test_card_to_default_place(self):
        Place.card_to_default_place(self.card, nb=3)


class TestBaskets(TestCase):

    def setUp(self):
        # Create a Card, a Basket and the "auto_command" Basket.
        self.basket = Basket(name="test basket")
        self.basket.save()
        self.basket_commands, created = Basket.objects.get_or_create(name="auto_command")
        self.basket_commands.save()

        # Create a Basket of type "box".
        self.box = Basket(name="box", is_box=True)
        self.box.save()

        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.card = Card(title="test card")
        self.card.save()
        self.nb_copies = 9

        # a Distributor
        self.distributor = DistributorFactory()
        self.distributor.save()

        # Preferences
        self.place = PlaceFactory.create()
        self.preferences = Preferences(default_place=self.place).save()

    def tearDown(self):
        log.setLevel(logging.DEBUG)

    def test_basket_add_copy(self):
        # add a card.
        self.basket.add_copy(self.card)  # it creates the intermediate table if not found.
        self.assertEqual(self.basket.basketcopies_set.get(card=self.card).nb, 1)
        # idem, with specific nb.
        self.basket.add_copy(self.card, nb=self.nb_copies)
        self.assertEqual(self.basket.basketcopies_set.get(card=self.card).nb, 1 + self.nb_copies)

    def test_box_add_copy(self):
        nb=3
        self.box.add_copy(self.card, nb=nb)  # it creates the intermediate table if not found.
        # quantity in stock changed: it's a box, not a basket.
        self.assertEqual(-nb, self.card.quantity)
        # again.
        self.box.add_copy(self.card, nb=3)
        self.assertEqual(-(nb + 3), self.card.quantity)

    def test_basket_add_copies(self):
        self.basket.add_copies([1])
        qt = self.basket.quantity(card_id=1)
        self.assertEqual(qt, 1)
        self.assertEqual(1, self.basket.quantity())

    def test_sell_auto_command_add_to_basket(self):
        """When a card reaches the threshold (0), pertains to a deposit and
        the deposit's auto_command is set to True, then add this card to the
        appropriate basket.
        """
        Card.sell(id=self.card.id)
        # fix: create the initial basket
        # self.assertEqual(self.basket_commands.basketcopies_set.get(card=self.card).nb, 1)

    # def test_to_deposit_nominal(self):
    #     """
    #     """
    #     # add a card
    #     self.card.distributor = self.distributor
    #     self.card.save()
    #     self.basket.add_copy(self.card)
    #     # add a Distributor
    #     dep, msgs = self.basket.to_deposit(self.distributor, name="depo test")
    #     self.assertFalse(msgs)


class TestBoxes(TestCase):

    def setUp(self):
        self.basket = Basket.new(name="test")
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.card = Card(title="test card")
        self.card.save()
        self.card2 = Card(title="test card")
        self.card2.save()

    def tearDown(self):
        pass

    def test_box_nominal(self):
        box = Basket.new(name="box", box=True)
        test = Basket.new(name="test", box=True)
        self.assertTrue(box)
        self.assertTrue(test)

    def test_box_archive(self):
        # Archive and put the books back in stock.
        box, status, messages = Basket.new(name="box", box=True)
        box.add_copy(self.card)
        box.add_copy(self.card2)
        self.assertEqual(-1, self.card.quantity)
        box.archive()
        # The book is still out of stock. We did NOT re-add it.
        self.assertEqual(-1, self.card.quantity_compute())

    def test_box_empty(self):
        # Archive and put the books back in stock.
        box, status, messages = Basket.new(name="box", box=True)
        box.add_copy(self.card)
        box.add_copy(self.card2)
        self.assertEqual(-1, self.card.quantity)
        box.empty()
        # The book is still out of stock. We did NOT re-add it.
        self.assertEqual(0, self.card.quantity_compute())


class TestReturnBaskets(TestCase):

    def setUp(self):
        # Create a Card, a ReturnBasket and the "auto_command" Basket.
        self.returnbasket = ReturnBasket(name="test return basket")
        self.returnbasket.save()
        self.basket_commands, created = Basket.objects.get_or_create(name="auto_command")
        self.basket_commands.save()
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.card = Card(title="test card")
        self.card.save()
        self.nb_copies = 9

        # a Distributor
        self.distributor = DistributorFactory()
        self.distributor.save()

        # Preferences
        self.place = PlaceFactory.create()
        self.preferences = Preferences(default_place=self.place).save()

    def tearDown(self):
        log.setLevel(logging.DEBUG)

    def test_add_copy(self):
        self.assertTrue(self.returnbasket)
        # Add a card in the stock.
        self.place.add_copy(self.card, nb=self.nb_copies)
        self.assertEqual(self.card.quantity, self.nb_copies)

        # Add the card to the return basket.
        ret = self.returnbasket.add_copy(self.card)
        self.assertTrue(ret)

        # The card should be decremented from the stock.
        self.assertEqual(self.card.quantity, self.nb_copies - 1)

        # Same, with a custom quantity.
        ret = self.returnbasket.add_copy(self.card, nb=3)
        self.assertEqual(self.card.quantity, self.nb_copies - 1 - 3)

    def test_add_copies(self):
        # Add a second card in the stock.
        card2 = Card(title="second card")
        card2.save()
        self.place.add_copy(self.card, nb=self.nb_copies)
        self.place.add_copy(card2, nb=1)
        self.assertEqual(card2.quantity, 1)

        # Add the two cards to the return basket.
        ret = self.returnbasket.add_copies([self.card.id, card2.id])
        self.assertTrue(ret)

        # The cards should have been decremented from the stock.
        # XXX: .quantity only is out of sync, even though card.save is called
        # and its quantity re-computed.
        self.assertEqual(card2.quantity_compute(), 1 - 1)
        # Or, re-get the card:
        obj2 = Card.objects.get(id=card2.id)
        self.assertEqual(obj2.quantity, 1 - 1)
        self.assertEqual(self.card.quantity_compute(), self.nb_copies - 1)

    def test_remove_copy(self):
        # Add a second card in the stock.
        card2 = Card(title="second card")
        card2.save()
        self.place.add_copy(self.card, nb=self.nb_copies)
        self.place.add_copy(card2, nb=3)  # 3
        self.assertEqual(card2.quantity, 3)

        # Add the two cards to the return basket.
        ret = self.returnbasket.add_copy(self.card, nb=1)
        ret = self.returnbasket.add_copy(card2, nb=3)
        self.assertTrue(ret)

        # Remove a card from the return basket: it should be back in stock.
        self.returnbasket.remove_copy(self.card.id)
        self.assertEqual(self.card.quantity_compute(), self.nb_copies)
        # again: the card isn't in the basket anymore, so no effect.
        self.returnbasket.remove_copy(self.card.id)
        self.assertEqual(self.card.quantity_compute(), self.nb_copies)

        # Re-put card2
        self.assertEqual(card2.quantity_compute(), 0)
        self.returnbasket.remove_copy(card2.id)
        self.assertEqual(card2.quantity_compute(), 3)

    def test_remove_copies(self):
        # Add a second card in the stock.
        card2 = Card(title="second card")
        card2.save()
        self.place.add_copy(self.card, nb=self.nb_copies)
        self.place.add_copy(card2, nb=1)
        self.assertEqual(card2.quantity, 1)

        # Add the two cards to the return basket.
        ret = self.returnbasket.add_copies([self.card.id, card2.id])
        self.assertTrue(ret)

        # Re-put them in stock:
        self.assertEqual(self.card.quantity_compute(), self.nb_copies - 1)
        self.returnbasket.remove_copies([self.card.id, card2.id])
        self.assertEqual(self.card.quantity_compute(), self.nb_copies)
        self.assertEqual(card2.quantity_compute(), 1)


class TestSells(TestCase):

    def setUp(self):
        # Create card types
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        # create an author
        self.GOLDMAN = "Emma Goldman"
        self.goldman = Author(name=self.GOLDMAN)
        self.goldman.save()
        # create a Card
        self.autobio = CardFactory(card_type=typ)
        self.autobio.authors.add(self.goldman)
        # a second card:
        self.secondcard = CardFactory()
        # a needed place:
        self.place_name = "test place"
        self.place = Place(name=self.place_name, is_stand=False, can_sell=True)
        self.place.save()

        # Add our books in stock to the default place.
        self.place.add_copy(self.autobio, nb=1)
        self.place.add_copy(self.secondcard, nb=1)

        # a Distributor:
        self.dist = Distributor(name="dist test")
        self.dist.save()
        # a Publisher:
        self.pub = Publisher(name="pub test")
        self.pub.save()
        # Preferences.
        self.preferences = Preferences(default_place=self.place).save()

    def tearDown(self):
        pass

    def test_sell_many_cards(self):
        p1 = 7.7
        p2 = 9.9
        to_sell = [{"id": self.autobio.id,
                    "quantity": 1,
                    "price_sold": p1},
                   {"id": self.secondcard.id,
                    "quantity": 2,
                    "price_sold": p2}]
        raw_to_sell = "1,7.7,1;2,9.9,2"
        self.assertEqual(self.place.quantity_of(self.autobio), 1)
        # Sell the 2 books.
        # revenue => 7.7 + 2 * 9.9
        sell, status, msgs = Sell.sell_cards(
            to_sell,
            raw_to_sell=raw_to_sell,
        )
        self.assertEqual(self.place.quantity_of(self.autobio), 0)
        self.assertEqual(ALERT_SUCCESS, status)

        # Simple check we save some fields.
        self.assertEqual(sell.raw_to_sell, raw_to_sell)

        soldcards = sell.soldcards_set.all()
        self.assertEqual(len(soldcards), 2)
        # Check prices
        self.assertEqual(soldcards[0].price_sold, p1)
        self.assertEqual(soldcards[1].price_sold, p2)
        # Check quantities
        self.assertEqual(soldcards[0].card.quantity, 0)
        self.assertEqual(soldcards[1].card.quantity, -1)
        # Check quantities through Card.quantity
        self.assertEqual(self.autobio.quantity_compute(), 0)

        # Sell again:
        # revenue => (7.7 + 2 * 9.9) * 2
        sell, status, msgs = Sell.sell_cards(to_sell)
        self.assertEqual(self.autobio.quantity_compute(), -1)
        self.assertEqual(self.place.quantity_of(self.autobio), -1)

        # Stats.
        stats = Stats.sells_month()
        self.assertEqual(2 * 3, stats['nb_cards_sold'])
        self.assertEqual(2, stats['nb_sells'])
        self.assertEqual(2 * (p1 + 2 * p2), stats['revenue'])

        # Check order.
        # It can be fucked up yeah, with ids_prices_quantities, pk__in etc.

    def test_sells_order(self):
        p1 = 7.7
        p2 = 9.9
        p3 = 20
        self.thirdcard = CardFactory()
        self.thirdcard.price = p3
        self.thirdcard.save()
        to_sell = [{"id": "1",
                    "quantity": 1,
                    "price_sold": p1},
                   # order not following the DB: here card 3.
                   # because a search like pk__in=ids doesn't respect order.
                   {"id": "3",
                    "quantity": 3,
                    "price_sold": p3},
                   {"id": self.secondcard.id,
                    "quantity": 2,
                    "price_sold": p2},
                   ]
        raw_to_sell = "1,7.7,1;2,9.9,2"
        # Sell the 3 books.
        sell, status, msgs = Sell.sell_cards(
            to_sell,
            raw_to_sell=raw_to_sell,
        )
        soldcards = SoldCards.objects.all()
        self.assertEqual(soldcards[0].card.id, 1)
        self.assertEqual(soldcards[0].quantity, 1)
        self.assertEqual(soldcards[0].price_sold, p1)
        # card 3 was sold
        self.assertEqual(soldcards[1].card.id, 3)
        self.assertEqual(soldcards[1].quantity, 3)
        self.assertEqual(soldcards[1].price_sold, p3)
        # card 2
        self.assertEqual(soldcards[2].card.id, 2)
        self.assertEqual(soldcards[2].quantity, 2)
        self.assertEqual(soldcards[2].price_sold, p2)

    def test_sell_none(self):
        to_sell = []
        sell, status, msgs = Sell.sell_cards(to_sell, silence=True)
        self.assertEqual(ALERT_WARNING, status)

    def test_sell_no_price(self):
        """
        Missing a price_sold => now errors out.
        """
        # no price_sold:
        # p1 = 7.7
        p2 = 9.9
        self.autobio.price = None
        self.autobio.save()
        to_sell = [{"id": self.autobio.id,
                    "quantity": 1,
                    # "price_sold": p1
                },
                   {"id": self.secondcard.id,
                    "quantity": 2,
                    "price_sold": p2}]

        sell, status, msgs = Sell.sell_cards(to_sell, silence=True)
        self.assertEqual(ALERT_ERROR, status)
        # int_table = sell.soldcards_set.all()
        # self.assertTrue(len(int_table), 1)

    def test_sell_place_id(self):
        # status, msgs = Card.sell(id=self.autobio.id, place_id=1)
        status, msgs = Card.sell(id=self.autobio.id)
        self.assertEqual(self.place.quantity_of(self.autobio), 0)

        # bad place_id
        status, msgs = Card.sell(id=self.autobio.id, place_id=9, silence=True)


    def test_sell_from_place(self):
        """
        Sell from a place, don't count it in the deposit,
        """
        # Sell.
        p1 = 7.7
        # p2 = 9.9
        to_sell = [{"id": self.autobio.id,
                    "quantity": 1,
                    "price_sold": p1}]
        # Sell for the place.
        Sell.sell_cards(to_sell, place_id=self.place.id)

    def test_undo_card(self):
        """Undo a sell, test only the Card method.
        """
        # Sell a card
        p1 = 7.7
        p2 = 9.9
        to_sell = [{"id": self.autobio.id,
                    "quantity": 1,
                    "price_sold": p1
                },
                   {"id": self.secondcard.id,
                    "quantity": 2,
                    "price_sold": p2}]
        sell, status, msgs = Sell.sell_cards(to_sell)

        # undo from Card:
        status, msgs = self.autobio.sell_undo()
        self.assertEqual(self.autobio.quantity, 1)
        status, msgs = self.autobio.sell_undo()
        # self.assertTrue(msgs)

    # DEPRECATED <2024-05-09>
    # We deprecate this: we actually don't need multiple storage places,
    # only multiple selling points, used for numbers at the end of the day.
    # Books are in the same "place"
    # (and still in different shelves anyways).
    # def test_undo_from_place(self):
    #     """
    #     Undo a sell, put it back on the original place.
    #     """
    #     # Create a second place.
    #     self.secondplace = Place(name="second place", is_stand=False, can_sell=True)
    #     self.secondplace.save()
    #     self.secondplace.add_copy(self.autobio)

    #     to_sell = [{"id": self.autobio.id,
    #                 "quantity": 1,
    #                 "price_sold": 7.7,
    #                 },
    #                {"id": self.secondcard.id,
    #                 "quantity": 2,
    #                 "price_sold": 9.9,
    #                 }]
    #     # Sell, set a place.
    #     sell, status, msgs = Sell.sell_cards(to_sell, place=self.secondplace)

    #     # undo the sell, put it back on the original place.
    #     self.assertEqual(self.secondplace.quantity_of(self.autobio), 0)
    #     sell.undo()
    #     # TODO: add place to Sell.to_list
    #     self.assertEqual(self.secondplace.quantity_of(self.autobio), 1)

    def test_undo_sell(self):
        """
        """
        # Sell a card
        p1 = 7.7
        p2 = 9.9
        to_sell = [{"id": self.autobio.id,
                    "quantity": 1,
                    "price_sold": p1
                },
                   {"id": self.secondcard.id,
                    "quantity": 2,
                    "price_sold": p2}]
        sell, status, msgs = Sell.sell_cards(to_sell)
        # complete undo from Sell:
        sell.undo()

        self.assertEqual(self.autobio.quantity_compute(), 1)
        self.assertEqual(self.autobio.quantity, 1)

    def test_undo_soldcard(self):
        """
        Undo only a soldcard, not a whole sell with many cards.
        """
        p1 = 7.7
        p2 = 9.9
        to_sell = [{"id": self.autobio.id,
                    "quantity": 1,
                    "price_sold": p1
                },
                   {"id": self.secondcard.id,
                    "quantity": 2,
                    "price_sold": p2}]
        sell, status, msgs = Sell.sell_cards(to_sell)

        # undo
        status, msgs = SoldCards.undo(2)
        self.assertEqual(True, status)
        self.assertEqual(SoldCards.objects.count(), 2)  # the soldcard object is still history.
        self.assertEqual(SoldCards.objects.first().card_id,
                         self.autobio.id)


    def test_history_suppliers(self):
        # A card gets both a dist and a pub:
        self.autobio.distributor = self.dist
        self.autobio.save()
        self.autobio.publishers.add(self.pub)
        # The secondcard gets a pub (the same):
        self.secondcard.publishers.add(self.pub)
        # A third card gets nothing.
        thirdcard = CardFactory()
        # We sell the three:
        Sell.sell_cards(None, cards=[self.autobio, self.secondcard, thirdcard])
        # We get our history for distributors and publishers:
        now = timezone.now()
        history = Sell.history_suppliers(year=now.year, month=now.month)

        # The first card was counted for the distributors and NOT for the publishers,
        # even though it has both information.
        self.assertEqual(1, history['distributors_data'][0]['nb_cards_sold'])
        # The thirdcard does not appear here.

    def test_best_sells(self):
        Sell.sell_card(self.autobio)
        res = get_best_sells(SoldCards.objects.all())
        self.assertTrue(res)
        self.assertEqual(1, len(res['book']))


class TestSellSearch(TestCase):

    # fixtures = ['test_sell_search']

    def setUp(self):
        # mandatory: unknown card type
        typ = CardType(name="unknown")
        typ.save()
        # create a Card
        self.autobio = CardFactory()
        # a second card:
        self.secondcard = CardFactory()
        self.secondcard.price = 20
        self.secondcard.save()
        # a needed place:
        self.place = PlaceFactory()
        self.place.add_copy(self.autobio, nb=1)
        self.place.add_copy(self.secondcard, nb=1)
        # a Distributor:
        self.dist = DistributorFactory()
        # Preferences
        self.preferences = Preferences.objects.create(default_place=self.place).save()

    def tearDown(self):
        log.setLevel(logging.INFO)

    def test_search_sells_distributor(self):
        # sell cards, only one of wanted distributor.
        self.autobio.distributor = self.dist
        self.autobio.save()
        Sell.sell_cards(None, cards=[self.autobio, self.secondcard])
        sells = Sell.search(distributor_id=self.dist.id)
        sells = sells['data']
        self.assertEqual(len(sells), 1)

        self.secondcard.distributor = self.dist
        self.secondcard.save()
        Sell.sell_card(self.secondcard)
        sells = Sell.search(distributor_id=self.dist.id, to_list=True)
        self.assertEqual(len(sells['data']), 3)

    def test_search_sells_card(self):
        Sell.sell_card(self.autobio)
        Sell.sell_card(self.secondcard)
        sells = Sell.search(card_id=self.autobio.id)
        self.assertEqual(len(sells['data']), 1)

        # Test order and sortby.
        now = timezone.now()
        res = Sell.search(date_max=now)
        res_sorted = Sell.search(date_max=now, sortorder=0)
        self.assertEqual(res['data'][0].card.id, res_sorted['data'][0].card.id)

        res_inversed = Sell.search(date_max=now, sortorder=1)
        self.assertEqual(res['data'][0].card.id, res_inversed['data'][1].card.id)

        res_price = Sell.search(date_max=now, sortby="price")
        self.assertTrue(res_price['data'][0].price_sold > res_price['data'][1].price_sold)
        res_price = Sell.search(date_max=now, sortby="price", sortorder=1)
        self.assertTrue(res_price['data'][0].price_sold < res_price['data'][1].price_sold)
        # for coverage...
        Sell.search(date_max=now, sortby="created", page_size="10")
        Sell.search(date_max=now, sortby="title", page_size=10, page=1)
        Sell.search(date_max=now, sortby="sell__id")
        Sell.search(date_max=now, count=True)
        log.setLevel(logging.CRITICAL)
        Sell.search(date_max=now, sortby="warning")

    def test_search_total_with_different_payments(self):
        # Sell a card with two payment methods, including a coupon:
        # the coupon should not be counted in the report.
        # note: don't rely on common.PAYMENT_CHOICES as it is changes when
        # loading config.py
        sell, created, alerts = Sell.sell_card(
            self.autobio,
            payment=100,   # coupon
            payment_2=1,
            total_payment_1=5,    # will be ignored in the report.
            total_payment_2=4.90,
        )
        sell_data = Sell.search(date_max=timezone.now(), with_total_price_sold=True)
        # 5€ of coupon is NOT counted in the report.
        self.assertEqual(4.90, sell_data['total_price_sold'])

        # Do the same, with multiple cards, check the count…
        #
        # The issue is that when counting the total per payment method,
        # we iterate on soldcards, but we have to check data on the mother Sell object,
        # so we must not count a Sell amount twice for related soldcards…
        sell, created, alerts = Sell.sell_cards(
            [
                {'id': self.autobio.pk, 'price_sold': 9.90, 'quantity': 1},
                {'id': self.secondcard.pk, 'price_sold': 20, 'quantity': 1},
            ],
            payment=100,
            payment_2=1,
            total_payment_1=20,    # will be ignored in the report.
            total_payment_2=9.90,
        )
        sell_data = Sell.search(date_max=timezone.now(), with_total_price_sold=True)
        self.assertEqual(4.90 + 9.90, sell_data['total_price_sold'])


    def test_search_sells_dates(self):
        """
        Search with a min date.
        """
        Sell.sell_card(self.autobio)
        Sell.sell_card(self.secondcard, date=timezone.now() - timezone.timedelta(days=30))
        sells = Sell.search(date_min=timezone.now() - timezone.timedelta(days=7))
        self.assertEqual(len(sells['data']), 1)


# DEPRECATED. Deletion date: beginning of 2024.
class TestInventory(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.place = PlaceFactory()
        self.card = CardFactory()
        # The place has a card before the inventory. But after the
        # inventory is applied, the place has what was in it.
        self.place.add_copy(self.card)
        self.inv = InventoryFactory()
        self.inv.place = self.place
        self.inv.save()

        # Preferences.
        self.preferences = Preferences(default_place=self.place).save()

    def test_inventory_state(self):
        self.inv.add_copy(self.card, nb=2)
        state = self.inv.state()
        self.assertEqual(state['nb_copies'], 2)
        self.assertEqual(state['nb_cards'], 1)

    def test_add_copy(self):
        res = self.inv.add_copy(self.card, nb=2)
        self.assertTrue(res)
        ic = self.inv.inventorycopies_set.get(card_id=self.card.id)
        self.assertEqual(ic.quantity, 2)

    def test_add_pairs(self):
        pairs = []
        status, msgs = self.inv.add_pairs(pairs)
        pairs = [[1, 3]]
        status, msgs = self.inv.add_pairs(pairs)
        pairs = [[1, 1]]
        status, msgs = self.inv.add_pairs(pairs)
        # add_pairs *sets* the quantities
        state = self.inv.state()
        self.assertEqual(state['nb_copies'], 1)

    def test_diff(self):

        self.card2 = CardFactory()
        self.card3 = CardFactory()
        self.place.add_copy(self.card2)
        self.inv.add_copy(self.card2, nb=2)
        self.inv.add_copy(self.card3, nb=1)
        # the inventory...
        d_diff, objname, _, _, _ = self.inv.diff()
        # - ... has not the card 1
        self.assertEqual(d_diff[1]['stock'], 1)
        self.assertEqual(d_diff[1]['inv'], 0)
        self.assertEqual(d_diff[1]['diff'], 1)
        self.assertEqual(d_diff[1]['in_orig'], True)

        # - has card2 with +1 copy
        self.assertEqual(d_diff[2]['stock'], 1)
        self.assertEqual(d_diff[2]['inv'], 2)
        self.assertEqual(d_diff[2]['diff'], -1)

        # - has card3 that the place doesn't have
        self.assertEqual(d_diff[3]['diff'], 1)
        self.assertEqual(d_diff[3]['inv'], 1)
        self.assertEqual(d_diff[3]['in_orig'], False)
        self.assertEqual(d_diff[3]['in_inv'], True)

class TestPreferences(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book", vat=5.5)
        self.preferences = PreferencesFactory()
        self.preferences.default_place = PlaceFactory()
        self.preferences.save()
        self.new_place = PlaceFactory()

    def tearDown(self):
        pass

    def test_set_preferences(self):
        status, msgs = Preferences.setprefs(default_place=self.new_place)
        self.assertEqual(status, ALERT_SUCCESS)

        prefs = Preferences.prefs()
        place = prefs.default_place

        self.assertEqual(place, self.new_place)

    def test_set_vat(self):
        vat = 2
        status, msgs = Preferences.setprefs(vat_book=vat)
        self.assertEqual(status, ALERT_SUCCESS)
        self.assertEqual(Preferences.prefs().vat_book, vat, msgs)

    def test_price_excl_tax(self):
        self.assertEqual(Preferences.price_excl_tax(10), 9.48)


class TestReception(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.card = CardFactory()
        self.shelf = ShelfFactory()
        # A default place in Preferences
        self.preferences = PreferencesFactory()
        self.preferences.default_place = PlaceFactory()
        self.preferences.save()

    def tearDown(self):
        pass

    def test_initial(self):
        self.assertTrue(Reception.ongoing())
        self.assertTrue(Reception.validate())
        self.assertTrue(Reception.ongoing())
        self.assertEqual(0, len(Reception.copies()))

        reception = Reception.ongoing()
        # Add
        status, msgs = reception.add_copy(1, shelf_id=self.shelf.id, nb=1)
        self.assertTrue(status)
        self.assertEqual(1, len(Reception.copies()))
        self.assertEqual(1, self.preferences.default_place.quantity_of(self.card))
        self.assertEqual(1, len(Reception.copies(to_list=True)))
        # Set.
        status, msgs = reception.add_copy(1, shelf_id=self.shelf.id, nb=3, set_quantity=True)
        self.assertTrue(status)
        self.assertEqual(1, len(Reception.copies()))
        self.assertEqual(3, self.preferences.default_place.quantity_of(self.card))
        self.assertEqual(1, len(Reception.copies(to_list=True)))


class TestStats(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        CardType.objects.create(name="book", vat=5.5)
        self.card = CardFactory.create()
        self.card2 = CardFactory.create()
        self.shelf = ShelfFactory.create()

    def test_stats(self):
        """Dummy "it compiles" tests."""
        # "Stats".
        Stats.stock()
        Stats.stock_age(1)
        Stats.entries_month()
        Stats.sells_month()

class TestBills(TestCase):
    def setUp(self):
        # Create one needed "book" card type.
        self.type_book = CardType.objects.create(name="book", vat=5.5)
        self.type_other = CardType.objects.create(name="other", vat=20)
        self.card = CardFactory.create()
        self.card2 = CardFactory.create()
        self.card2.card_type = self.type_other
        self.card2.isbn = None  # in case save() changes the type
        self.card2.save()
        self.card3 = CardFactory.create()
        self.card3.isbn = None
        self.card3.card_type = self.type_other
        self.card3.save()

    def tearDown(self):
        pass

    def test_get_vat_values(self):
        #################################################
        # Selling a book VAT 5.5 and another VAT 20%.
        #################################################
        res = Card.get_vat_values([(self.card, 1, 9.99),  # book
                                   (self.card2, 1, 9.99), # other
                                   ])
        for tup in res:
            self.assertTrue(tup[1] > 0, "vat part failed for {}, we prob' shouldn't have 0".format(tup))
        total_vats = res[0][1] + res[1][1]
        self.assertTrue(2.17 <= total_vats < 2.1801)

        # Same, with discounts.
        prices_discounted = [8, 8]
        res = Card.get_vat_values([(self.card, 1, 9.99),  # book
                                   (self.card2, 1, 9.99), # other
                                   ],
                                  prices_discounted=prices_discounted)
        for tup in res:
            self.assertTrue(tup[1] > 0, "vat part failed for {}, we prob' shouldn't have 0".format(tup))
            # VATs: 20% of 8 EUR -> 1.33 and 5.5% of 8 EUR -> 0.42
        total_vats = res[0][1] + res[1][1]
        self.assertTrue(1.75 <= total_vats < 1.76)

        #################################################
        # Again, with 2 cards of "other" type at 20% VAT.
        #################################################
        res = Card.get_vat_values([(self.card2, 1, 9.99),
                                   (self.card3, 1, 9.99),
                                   ])
        self.assertEqual(3.33, res[0][1])

        # Same, with discounts.
        res = Card.get_vat_values([(self.card2, 1, 9.99),
                                   (self.card3, 1, 9.99),
                                   ],
                                  prices_discounted=prices_discounted)
        self.assertEqual(2.67, res[0][1])
