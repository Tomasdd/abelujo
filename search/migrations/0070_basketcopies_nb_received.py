# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0069_auto_20230616_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='basketcopies',
            name='nb_received',
            field=models.IntegerField(default=0),
        ),
    ]
