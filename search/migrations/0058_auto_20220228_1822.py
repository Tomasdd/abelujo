# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0057_auto_20220216_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='comment',
            field=models.TextField(null=True, verbose_name='comment', blank=True),
        ),
    ]
