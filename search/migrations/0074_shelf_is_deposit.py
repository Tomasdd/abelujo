# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0073_basket_sent_to_command_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='shelf',
            name='is_deposit',
            field=models.BooleanField(default=False, verbose_name='Is it a deposit?'),
        ),
    ]
