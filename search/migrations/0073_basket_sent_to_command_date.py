# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0072_auto_20231026_1754'),
    ]

    operations = [
        migrations.AddField(
            model_name='basket',
            name='sent_to_command_date',
            field=models.DateField(null=True, verbose_name='the date we sent the books to the command list.', blank=True),
        ),
    ]
