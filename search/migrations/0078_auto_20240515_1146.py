# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0077_auto_20240504_1633'),
    ]

    operations = [
        migrations.AddField(
            model_name='sell',
            name='raw_to_sell',
            field=models.CharField(max_length=10000, null=True, blank=True),
        ),
    ]
