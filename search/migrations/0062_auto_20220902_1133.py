# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0061_auto_20220614_2249'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookshop',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2022, 9, 2, 11, 32, 47, 657052, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bookshop',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2022, 9, 2, 11, 33, 7, 203329, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='client',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2022, 9, 2, 11, 33, 27, 614017, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='client',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2022, 9, 2, 11, 33, 34, 790192, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
