# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Stripe payments OR simple book reservations without payments.

The website first calls /api/checkout => api_stripe which does some checks and calls handle_api_stripe

If it's a payment, then Stripe needs to call web hooks => api_stripe_hooks
"""

from __future__ import unicode_literals

import httplib
import json
import logging
from pprint import pprint

import requests
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from abelujo import settings
from search import mailer
from search.models import Card
from search.models import Basket
from search.models import Client
from search.models import Reservation
from search.models.utils import _is_truthy
from search.models.utils import get_logger
from search.models.utils import get_total_weight
from search.views_utils import bulk_import_from_dilicom

# from search.views_utils import search_on_data_source_with_cache  # replaced by bulk search.

logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s:%(lineno)s]:%(message)s', level=logging.DEBUG)
log = get_logger()

def api_sofia_post(request, *arg, **kw):
    params = request.GET.copy()
    basket_id = params.get('basket')
    basket = None

    to_ret = {"status": 500}
    try:
        basket = Basket.objects.filter(id=basket_id).first()
    except Exception as e:
        log.error(e)
        return JsonResponse(to_ret)

    try:

        # copies =  [it.to_dict() for it in basket.basketcopies_set.all()]
        copies =  [it.to_dict_light() for it in basket.basketcopies_set.all()]
        json_cards = json.dumps(copies)
        import ipdb; ipdb.set_trace()
        data = {
            'cards': json_cards,
            'bookshop': {},  # TODO:
            'client': {},  # TODO:
        }
    except Exception as e:
        log.error(e)

    URL = 'http://127.0.0.1:4567'
    POST_URL = URL + '/sofia'

    try:
        ping = requests.get(URL)
        if ping.status_code != 200:
            log.error("SOFIA: no local server.")
            return JsonResponse(to_ret)

        req = requests.post(POST_URL, json=json_cards)

        if req.status_code != 200:
            log.error("SOFIA: processing error.")
            return JsonResponse(to_ret)

    except Exception as e:
        log.error(e)
        return JsonResponse(to_ret)

    print(req.text)
    return JsonResponse({'status': 200})
