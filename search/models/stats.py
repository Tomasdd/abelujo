# -*- coding: utf-8 -*-

"""
Global stats.
"""
from __future__ import unicode_literals

# __unicode__ is now __str__
from django.utils.encoding import python_2_unicode_compatible
from django.utils import six

import calendar
import datetime
import locale
import json
import os
import tempfile
from cachetools import cached, TTLCache
from datetime import date
from toolz.itertoolz import groupby

import pendulum

from django.utils.translation import ugettext as _  # in functions.
from django.utils.translation import ugettext_lazy as __  # in Meta and model fields.

from abelujo import settings
from search.models import Card
from search.models import EntryCopies
from search.models import Shelf
from search.models import Sell
from search.models import SoldCards
from search.models import shelf_age_sort_key
from search.models.common import ALERT_ERROR
from search.models.common import ALERT_INFO
from search.models.common import ALERT_SUCCESS
from search.models.common import ALERT_WARNING
from search.models.common import CHAR_LENGTH
from search.models.common import CURRENCY_CHOICES
from search.models.common import DATE_FORMAT
from search.models.common import YMD_DATE_FORMAT
from search.models.common import PAYMENT_CHOICES
from search.models.common import TEXT_LENGTH
from search.models import utils

log = utils.get_logger()

def stock_age(to_dict_light=True):
    """
    Group all the cards in stock by age interval (0-3 months, 3-6, 6-12, 12-18, 18-24, >24).

    Return: dict: int -> list of simple card dicts (only id, title, price, quantity) (necessary for speed).
    """
    # very similar to Stats._shelf_age, but for all cards in stock.
    cards = Card.objects.filter(quantity__gt=0, isbn__isnull=False)
    # XXX: get only books.
    # cards = Card.objects.filter(quantity__gt=0)[:500]   # DEV # TODO:
    stats = groupby(shelf_age_sort_key, cards)

    # Have a dict with all keys (easier for plotting charts)
    for key in range(6):
        if key not in stats:
            stats[key] = []

    # to dict
    if to_dict_light:
        stats = valmap(lambda its: [it.to_dict_light() for it in its], stats)

    return stats

def merge_ages(stats):
    """
    Our categories 3, 4, 5 must be grouped into one (>12 months) for our official stats.
    """
    res = {}
    tmp_cards = []
    for age, cards in stats.items():
        if age in [0, 1, 2]:
            res[age] = cards
        else:
            tmp_cards += cards

    res[3] = tmp_cards
    return res

def stock_age_state(to_dict_light=True):
    """
    Same as above, augmented:
    - nb of references
    - nb of volumes
    - ratio volumes / references
    - stock value
    - % stock value

    And for those official stats, the last age is >12 months, so we need to merge our categories 3, 4 and 5.

    Return: dict int -> dict with keys: data (card objects), and the required numbers.
    """
    stats = stock_age(to_dict_light=to_dict_light)
    stats = merge_ages(stats)
    res = {}
    total_value = 0
    stats_values = {}  # age (int) -> value (float)

    # Get total value.
    for stat in stats.items():
        age = stat[0]
        cards = stat[1]
        value = 0
        for card in cards:
            if card.price and card.quantity:
                value += card.price * card.quantity

        total_value += value
        stats_values[age] = value

    # Get other numbers.
    for stat in stats.items():
        age = stat[0]
        cards = stat[1]
        nb_titles = len(stat[1])
        nb_copies = sum([it.quantity for it in stat[1]])
        if nb_copies and nb_titles:
            ratio_copies_titles = utils.roundfloat(nb_copies / float(nb_titles), rounding="ROUND_HALF_EVEN")
        else:
            ratio_copies_titles = 0

        value = utils.roundfloat(stats_values[stat[0]], rounding="ROUND_HALF_EVEN")
        value_percent = 0
        if value and total_value:
            value_percent = utils.roundfloat(value / total_value * 100, rounding="ROUND_HALF_EVEN")
        res[age] = {
            'nb_titles': nb_titles,
            'nb_copies': nb_copies,
            'ratio_copies_titles': ratio_copies_titles,
            'value': value,
            'value_percent': value_percent,
            # 'data': cards,
        }

        res[99] = {
            'comment': "Products with an ISBN in stock.",
        }

    return res

def _save_ventilation_in_cache(data=None, year=None, month=None, day=None):
    """
    Save this data for this year/month and optionaly day.

    For ventilation data, we only save by month.

    Return: boolean.
    """
    directory = settings.STATS_DIR
    if not os.path.exists(directory):
        # Shouldn't happen but hey...
        log.error("Could not save stats on disk: STATS_DIR doesn't exist.")
        return False
    # TODO:

def categories_ventilation(with_shelves=True, verbosity=0):
    """
    Allocation of books by category: nb of titles, of copies, %, value, % value, rotation, revenue of books.

    Also get the same numbers for: new parutions (<1 month) and old stock (>12 months).

    Params:
    - with_shelves: if not True, don't include data on shelves, include only data on new and old stock.
    - verbosity: if True, print on stdout. Useful when running ./manage.py stats_ventilation -v

    Return: xxx

    The official categories overlap shelves.
    For now, we get numbers on shelves, we don't try to match official categories.
    - Littérature
    - Policier/SF
    - Poésie
    - Théâtre
    - Jeunesse
    - BD
    - …

    """
    total_value_shelves = 0
    total_copies_shelves = 0
    to_ret = {}

    now = pendulum.now()
    min_date = now
    twelve_months_ago = now - now.subtract(months=12)
    one_month_ago = now - now.subtract(months=1)
    if isinstance(min_date, pendulum.Period):
        min_date = min_date.start
    if isinstance(twelve_months_ago, pendulum.Period):
        twelve_months_ago = twelve_months_ago.start
    if isinstance(one_month_ago, pendulum.Period):
        one_month_ago = one_month_ago.start

    # All entries since 1 year.
    # all_entries = EntryCopies.objects.filter(created__lte=min_date, created__gte=twelve_months_ago)

    # All sells since 1 year.
    # all_sells = SoldCards.objects.filter(created__lte=min_date, created__gte=twelve_months_ago)
    all_sells = SoldCards.objects.filter(created__lte=min_date, created__gte=twelve_months_ago)

    start = pendulum.now()
    vals = all_sells.values_list('price_sold', 'quantity')
    all_sells_total = sum([it[0] * it[1] for it in vals])
    end = pendulum.now()
    log.debug("--- all_sells_total took {}".format(end - start))

    shelves = Shelf.objects.all()

    def _print(s):
        if verbosity:
            print(s)

    def shelf_ventilation(shelf, all_sells=[]):
        to_ret = {
            'shelf': shelf.to_dict(),
            'shelf_id': shelf.pk,
        }
        cards = shelf.cards(in_stock=True)
        _print(shelf)
        # Number of cards.
        cards_qty = shelf.cards_qty
        to_ret['cards_qty'] = cards_qty
        # Number of copies (volumes).
        copies_qty = shelf.copies_qty
        to_ret['copies_qty'] = copies_qty
        # _print("cards: {} copies: {}".format(shelf.cards_qty, shelf.copies_qty))
        _print("références: {} volumes: {}".format(cards_qty, copies_qty))

        # Value.
        # _print("value: {}".format(shelf.value()))
        value = shelf.value()
        to_ret['value'] = value
        # to_ret['percent_value'] = XXX
        _print("valeur stock: {}".format(value))

        # Entries for this shelf.
        # entries = all_entries.filter(card__shelf=shelf)

        sells = all_sells.filter(card__shelf=shelf)
        vals = sells.values_list('price_sold', 'quantity')
        total_sells = sum([it[0] * it[1] for it in vals])
        to_ret['total_sells'] = total_sells
        # XXX: percent

        # Rotation:
        # value of sells / mean value of stock
        # for the mean value, we only take the current value, for now…
        rotation = utils.roundfloat(total_sells / value if value else 0, rounding="ROUND_HALF_EVEN")
        to_ret['rotation'] = rotation
        _print("rotation: {}".format(rotation))

        rotation_days = utils.roundfloat((value * 365) / total_sells if total_sells else 0, rounding="ROUND_HALF_EVEN")
        to_ret['rotation_days'] = rotation_days
        _print("rotation jours: {}".format(rotation_days))

        #
        # Same numbers, for new parutions (<1 month) and old stock (>12 months).
        #
        # new parutions: let's consider the ones published less than
        # one month ago *relatively to their date of entry in stock*.
        new_stock = []
        new_stock_data = {}
        new_stock = cards.filter(date_publication__gte=one_month_ago)
        new_stock_data['cards_qty'] = new_stock.count()
        new_stock_data['copies_qty'] = sum([it.quantity for it in new_stock])
        new_stock_data['value'] = sum([it.quantity * it.price for it in new_stock])

        new_stock_data['percent_copies'] = 0
        if copies_qty:
            percent_copies = utils.roundfloat(
                new_stock_data['copies_qty'] / float(copies_qty) * 100,
                rounding="ROUND_HALF_EVEN"
            )
            new_stock_data['percent_copies'] = percent_copies

        new_stock_data['percent_value'] = 0
        if value:
            percent_value = utils.roundfloat(new_stock_data['value'] / float(value) * 100, rounding="ROUND_HALF_EVEN")
            new_stock_data['percent_value'] = percent_value
        _print("nouveautés: références: {} volumes: {} % volumes: {} valeur: {} % valeur: {}".format(
            new_stock_data['cards_qty'],
            new_stock_data['copies_qty'],
            new_stock_data['percent_copies'],
            new_stock_data['value'],
            new_stock_data['percent_value'],
        ))
        to_ret['new_stock_data'] = new_stock_data

        #
        # Old stock
        #
        old_stock = []
        old_stock_data = {}
        old_stock = cards.filter(date_publication__gte=twelve_months_ago)
        old_stock_data['cards_qty'] = old_stock.count()
        old_stock_data['copies_qty'] = sum([it.quantity for it in old_stock])
        old_stock_data['value'] = sum([it.quantity * it.price for it in old_stock])

        old_stock_data['percent_copies'] = 0
        if copies_qty:
            percent_copies = utils.roundfloat(old_stock_data['copies_qty'] / float(copies_qty) * 100,
                                        rounding="ROUND_HALF_EVEN")
            old_stock_data['percent_copies'] = percent_copies

        old_stock_data['percent_value'] = sum([it.quantity * it.price for it in old_stock])
        if value:
            percent_value = utils.roundfloat(old_stock_data['value'] / float(value) * 100,
                                       rounding="ROUND_HALF_EVEN")
            old_stock_data['percent_value'] = percent_value

        _print("fonds: références: {} volumes: {} % volumes: {} valeur: {} % valeur: {}".format(
            old_stock_data['cards_qty'],
            old_stock_data['copies_qty'],
            old_stock_data['percent_copies'],
            old_stock_data['value'],
            old_stock_data['percent_value'],
        ))
        to_ret['old_stock_data'] = old_stock_data

        _print("")
        return to_ret
        ### end shelf_ventilation ###

    shelves_ventilation = []  # first sorted by …
    if with_shelves:
        for shelf in shelves:
        # for shelf in shelves[:2]:
            shelves_ventilation.append(shelf_ventilation(shelf, all_sells=all_sells))

        # Sort by rotation.
        shelves_ventilation = sorted(shelves_ventilation, key=lambda it: it['rotation'], reverse=True)

    #
    # Rotation of new stock
    # for all the stock, globally.
    # XXX: we compute the new stock rotation with this months' sells only,
    # so we compute "this month's stock value" with a mean: the total value, divided by 12.
    this_month_sells = all_sells.filter(created__gte=one_month_ago)
    # Sells of this month for new stock only:
    this_month_new_stock_sells = this_month_sells.filter(
        card__date_publication__gte=one_month_ago
    )
    vals = this_month_new_stock_sells.values_list('price_sold', 'quantity')
    this_month_new_stock_sells_total = utils.roundfloat(
        sum([it[0] * it[1] for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )

    # tmp vars, shorter.
    total_sells = this_month_new_stock_sells_total
    value = 0
    # value of new stock in total.
    new_stock = Card.objects.filter(date_publication__gte=one_month_ago)  # gte one_month_ago
    vals = new_stock.values_list('price', 'quantity')
    new_stock_value = utils.roundfloat(
        sum([it[0] * it[1] for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )
    value = new_stock_value / 12.0  # get a mean.

    new_stock_rotation = utils.roundfloat(
        total_sells / value if value else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("nouveautés rotation: {} (CA ce mois {} / valeur stock (moyenne mois) {})".format(
        new_stock_rotation,
        total_sells,
        value,
    ))

    new_stock_rotation_days = utils.roundfloat(
        (value * 365) / total_sells if total_sells else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("nouveautés rotation jours: {}".format(new_stock_rotation_days))
    # -- end rotation new stock --- #

    # ! copy-pasted from new stock.
    # Rotation of old stock.
    # We compute the old stock rotation with this months' old stock
    # THAT WAS SOLD THIS MONTH
    # only,
    # so we compute a mean of the old stock to get "this month's old stock".

    # this_month_sells = all_sells.filter(created__gte=one_month_ago)  # got it above.
    # Sells of this month for old stock only:
    this_month_old_stock_sells = this_month_sells.filter(
        card__date_publication__lte=twelve_months_ago  # here lte twelve_months_ago
    )
    vals = this_month_old_stock_sells.values_list('price_sold', 'quantity')
    this_month_old_stock_sells_total = utils.roundfloat(
        sum([it[0] * it[1] for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )

    # tmp vars, shorter.
    total_sells = this_month_old_stock_sells_total
    value = 0
    # value of old stock in total.
    old_stock = Card.objects.filter(date_publication__lte=twelve_months_ago)  # lte twelve_months_ago
    vals = old_stock.values_list('price', 'quantity')
    old_stock_value = utils.roundfloat(
        sum([ (it[0] or 0) * max(it[1], 0) for it in vals]),
        rounding="ROUND_HALF_EVEN"
    )
    value = old_stock_value / 12.0  # get a mean.

    old_stock_rotation = utils.roundfloat(
        total_sells / value if value else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("fonds rotation: {} (CA ce mois {} / valeur stock (moyenne mois) {})".format(
        old_stock_rotation,
        total_sells,
        value,
    ))

    old_stock_rotation_days = utils.roundfloat(
        (value * 365) / total_sells if total_sells else 0,
        rounding="ROUND_HALF_EVEN"
    )
    _print("fonds rotation jours: {}".format(old_stock_rotation_days))
    # -- end rotation old stock --- #

    # Save data for this month on disk.
    # save it on the DB? On files?
    # _save_ventilation_in_cache(now)

    res = {
        'new_stock_rotation': new_stock_rotation,
        'new_stock_rotation_days': new_stock_rotation_days,
        'old_stock_rotation': old_stock_rotation,
        'old_stock_rotation_days': old_stock_rotation_days,
        'shelves_ventilation': shelves_ventilation,
    }
    return res
