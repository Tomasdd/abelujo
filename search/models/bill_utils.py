# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
import os

import pendulum
from weasyprint import HTML

from django.http import JsonResponse
from django.template.loader import get_template
from django.utils import translation
from django.utils.translation import ugettext as _
from django.template.loader import get_template

from abelujo import settings
from search.models import Basket
from search.models import Bill
from search.models import Card
from search.models import CardType
from search.models import Client
from search.models import Preferences
from search.models import Sell
from search.models import users
from search.models import utils

from search.models.utils import price_fmt

log = utils.get_logger()


def create_bill(params, request_from_history=False, write_pdf=None, with_data_dict=False):
    """
    Create a bill or an estimate, as a PDF file.

    params: dict

    Create a bill from either:
    - the given products (list of ids)
      - each has an optional discount.
      - for the given client, or a temporary client (string).
    - cards_data: a list of triples card object, quantity, price
    - an existing bill id.
    - or a given basket id.
    - bill_or_estimate: 1 is bill, 2 is estimate, 3 is bill of the sell..
    - bon_de_commande: string

    3: bill of a sell: no need to generate a new unique Bill id. The document generated at the sell is not really a bill, as in "accounting document". It's only for the client.
    On the contrary, a bill generated for an institutional client in a List is an accounting document.

    For a shipping receipt ("bon de livraison"), use the views.py method, direct URL. See baskets.html.

    """
    template = 'pdftemplates/pdf-bill-main.html'

    language = params.get('language')
    if language:
        translation.activate(language)

    bill_or_estimate = params.get('bill_or_estimate', 1)  # bill
    # sellbooks = params.get('checkboxsell')  # a confirmation that we sell the books (from baskets).
    # sellbooks = is_truthy(sellbooks)

    show_payment_details = True

    # Creation date date.
    FILENAME_DATE_FMT = '%d-%m-%Y'  # to name the PDF file.
    INFILE_DATE_FMT = '%d/%m/%Y'    # to display in the PDF. Respect / convention for external tools.
    creation_date = pendulum.today()
    creation_date_label = _("Created")  # this can be in trans template tags.
    # We don't create "true" bills, only transaction justifications for clients.
    # We don't need the "due" date.
    # due_date
    # due_date_fmt
    # due_date_label = _("Due")

    cards = []
    sorted_cards = []
    cards_data = []  # list of triples: card object, quantity, price_sold.

    # payment_id = params.get('payment_id')
    # option 1: give a list of triples card, quantity, price.
    cards_data = params.get('cards_data', [])
    # option 2: give the triples separately.
    # They should always be inferred anyways.
    ids = params.get('ids', [])
    prices = params.get('prices', [])
    # This list of prices is used to compute the total.
    prices_sold = params.get('prices_sold', [])
    try:
        prices_sold = [utils.roundfloat(it) for it in prices_sold]
    except Exception as e:
        log.error("bill_utils: error trying to round a float from the client side prices_sold: {}".format(e))
    quantities = params.get('quantities', [])
    prices_discounted = params.get('prices_discounted', [])
    # Show another column "discounted price" on the bill, next to the public price.
    # This param is used wwith institutions,
    # but we use it with sells with a discount too.
    show_discounted_price = params.get('show_discounted_price')

    # A sell id (from history page).
    sell_id = 0
    try:
        sell_id = int(params.get('sell_id', 0))
    except Exception as e:
        log.debug(e)

    sell = None
    if sell_id:
        sell = Sell.objects.filter(id=sell_id).first()

    # Is the bill paid?
    # When getting the bill from a past sell, from the History, yes.
    # We can probably mark it paid in the Sell view too?
    is_paid = False
    if sell:
        is_paid = True

    # Sell date.
    sell_date = params.get('date')
    if sell_date:
        creation_date = pendulum.parse(sell_date)
    else:
        # During a sell that's unlikely, JS always sets the date.
        # But now we generate bills from the history view.
        if sell:
            # need a pendulum object for add() later.
            creation_date = pendulum.instance(sell.created)
        else:
            # Now this is unlikely.
            creation_date = pendulum.today()
    filename_creation_date_fmt = creation_date.strftime(FILENAME_DATE_FMT)
    infile_creation_date_fmt = creation_date.strftime(INFILE_DATE_FMT)

    # Client
    client_id = params.get('client_id')
    client = None
    if client_id:
        qs = Client.objects.filter(pk=client_id)
        if qs:
            client = qs.first()
    # Get the client from the sell.
    if sell and sell.client:
        client = sell.client

    # Temporary client? Only show on the bill, don't save in DB.
    temporary_client = params.get('temporary_client')

    # Bon de commande (order form ID), to show on the PDF.
    bon_de_commande = params.get('bon_de_commande_id')
    # Shipping cost, to add to the total to pay.
    shipping_cost = params.get('shipping_cost')
    shipping_cost_fmt = None
    shipping_cost_label = _("Shipping cost")
    if shipping_cost and utils.is_invalid(shipping_cost):
        shipping_cost = None

    discount_dict = params.get('discount', {})
    discount = 0
    discount_fmt = "0 %"
    show_discount = False
    DEFAULT_INSTITUTION_DISCOUNT = 9  # rather set on client save?
    if discount_dict:
        discount = discount_dict['discount']
        discount_fmt = discount_dict['name']
        show_discount = True
    if client and client.discount:
        discount_fmt = "{} %".format(client.discount)
        discount = client.discount
        show_discount = True
    elif client and client.is_institution:
        discount_fmt = "{} %".format(DEFAULT_INSTITUTION_DISCOUNT)
        discount = 9
        show_discount = True

    # Only apply the discount from the Sell view, not from the history.
    # Otherwise, it is counted twice: once during the sell, then here in the history.
    # But now, when generating the bill from the history, we can't tell
    # if the discount was applied.
    #
    # Workaround: don't have a client with "automatic discount", just apply it manually
    # at the sell.
    #
    # A better approach: save the information of the applied discount at the sell.
    if request_from_history:
        show_discount = False

    # A basket id.
    basket_id = int(params.get('basket_id', -1))

    # Other params.
    language = params.get('language')

    total = 0
    total_discounted = 0  # when a discount is applied at the sell page.

    if language:
        translation.activate(language)

    if cards_data:
        for triple in cards_data:
            cards.append(triple[0])
            ids.append(triple[0].pk)
            quantities.append(triple[1])
            prices.append(triple[2])
            prices_sold.append(triple[2])
            prices_discounted.append(triple[3])

    elif ids:
        # Cards
        cards = Card.objects.filter(pk__in=ids)
        # sort as in ids and quantities:
        sorted_cards = sorted(cards, cmp=lambda x, y: -1 if ids.index(x.pk) <= ids.index(y.pk) else 1)
        if show_discount:
            cards_data = list(zip(sorted_cards, quantities, prices, prices_sold))
            prices_discounted = prices_sold  # so we get the right VAT.
            show_discounted_price = True  # show it on the bill.

            # If we did set a discount, that means we sell the books at their normal price.
            # We can compute the totals here.
            # In a sell without discount, we must sum the prices sold to get the total of the sell,
            # because the card prices might have been manually corrected.
            # (same loop as below)
            for i, price in enumerate(prices):
                if price is not None and quantities[i] is not None:
                    total += price * quantities[i]
                    if prices_sold:
                        total_discounted += prices_sold[i] * quantities[i]

        else:
            cards_data = list(zip(sorted_cards, quantities, prices_sold))

    elif sell_id:
        cards = []
        if sell.soldcards_set.count():
            soldcards = sell.soldcards_set.all()
            cards = [it.card for it in soldcards]
            sorted_cards = cards
            ids = [it.pk for it in cards]
            prices = [it.price_sold for it in soldcards]
            prices_sold = [it.price_sold for it in soldcards]
            prices_sold = [roundfloat(it) for it in prices_sold]
            quantities = [it.quantity for it in soldcards]
            if show_discounted_price:
                cards_data = list(zip(sorted_cards, quantities, prices_sold, prices_discounted))
            else:
                cards_data = list(zip(sorted_cards, quantities, prices_sold))

    elif basket_id and basket_id != -1:
        try:
            basket = Basket.objects.filter(id=basket_id).first()
            basket_copies = basket.basketcopies_set.order_by('card__title').all()
            cards = [it.card for it in basket_copies]
            sorted_cards = cards
            ids = basket_copies.values_list('card__pk', flat=True)
            prices = basket_copies.values_list('card__price', flat=True)
            # prices_sold = cards.values_list('price_sold', flat=True)  # Not Available
            prices_sold = prices
            quantities = basket_copies.values_list('nb', flat=True)

            prices_discounted = []
            for it in prices:
                prices_discounted.append(utils.price_minus_percent(it, DEFAULT_INSTITUTION_DISCOUNT))

            if show_discounted_price:
                cards_data = list(zip(sorted_cards, quantities, prices, prices_discounted))
            else:
                cards_data = list(zip(sorted_cards, quantities, prices))
            if client_id:
                client_id = int(client_id)
                # client_discount = 0
            # sell the books?
            # if sellbooks:
            #     ids_prices_quantities = []
            #     try:
            #         for i, card in enumerate(cards):
            #             ids_prices_quantities.append({
            #                 'id': card.id,
            #                 'price_sold': prices[i],
            #                 'quantity': quantities[i],
            #             })
            #         now = timezone.now()
            #         sell, status, alerts = Sell.sell_cards(
            #             ids_prices_quantities,
            #             date=now,
            #             client_id=client_id,
            #         )
            #     except Exception as e:
            #         to_ret = {'status': 500,
            #                   'messages': ['An error occured trying to sell the books.']}
            #         log.error("Error selling cards for a bill: {}".format(e))
            #         return JsonResponse(to_ret)
        except Exception as e:
            to_ret = {'status': 500,
                      'messages': ['An error occured generating the bill.']}
            log.error("Error generating bill from basket: {}".format(e))
            # return JsonResponse(to_ret)
            return to_ret

    # The bookshop identity.
    bookshop = None
    bookshop_name = ""
    try:
        bookshop = users.Bookshop.objects.first()
        bookshop_name = bookshop.name if bookshop else ""
    except Exception as e:
        log.warning("Error getting users.Bookshop: {}".format(e))

    # Document title.
    if bill_or_estimate in [1, "1", u"1"]:
        document_type = _("Bill")
        # name, filename
        bill_label = document_type
    elif bill_or_estimate in [3, "3", u"3"]:
        document_type = _("Sell bill")  # "facture de caisse"
        bill_label = document_type
    elif bill_or_estimate in [2, "2", u"2"]:
        document_type = _("Estimate")
        bill_label = document_type
    else:
        document_type = _("Bill")
        bill_label = document_type

    # Unique ID.
    # Even if we don't use the Bill object, create one so we get unique IDs.
    bill_number = None
    if bill_or_estimate in [1, "1", u"1"]:
        bill_object = Bill(name="{}-{}".format(bookshop_name, filename_creation_date_fmt))
        bill_object.save()
        bill_number = bill_object.pk

    name = "{}-{}_{}-{}".format(bill_label,
                                bookshop_name,
                                bill_number if bill_number else "",
                                filename_creation_date_fmt)
    filename = name + '.pdf'

    document_title = "{} {}{}{}".format(document_type,
                                        bill_number if bill_number else "",
                                        "-" if bill_number else "",
                                       pendulum.now().strftime('%Y%m%d'))

    # File 2, with books list.
    # details_name = "{} {} - {} - list".format(bill_label, bookshop_name, creation_date_fmt)
    # details_filename = name + '.pdf'
    # details_template = "pdftemplates/pdf-bill-details.html"

    # Totals
    total_discounted_fmt = None
    total_with_client_discount = total
    if not (len(ids) == len(prices) == len(quantities)):  # prices_sold: not for basket(?)
        log.error("Bill: post params are malformed. ids, prices, prices_sold and quantities should be of same length.")
        to_ret = {'status': 500,
                  'messages': ['Bill parameters are malformed.']}
        return to_ret

    if not total:
        # for i, price in enumerate(prices):
        for i, price in enumerate(prices_sold):
            # XXX: check price type and value
            if price is not None and quantities[i] is not None:
                total += price * quantities[i]
                if prices_sold:
                    total_discounted += prices_sold[i] * quantities[i]

    default_currency = Preferences.get_default_currency()
    vat_book = Preferences.get_vat_book()
    total_fmt = price_fmt(total, default_currency)

    if show_discount:
        total_with_client_discount = total - total * discount / 100.0
        total_discounted = total_with_client_discount

    # if not prices_sold:
        # total_discounted = total_with_client_discount

    if not total_discounted:
        total_discounted = total

    total_discounted_fmt = price_fmt(total_discounted, default_currency)

    template = get_template(template)

    # Compute the VAT from the discounted price (after for example 9% discount to an official client).
    total_vat = 0
    vat_values = []  # list of tuples: vat, total for this vat %
    only_one_vat_type = False  # if True, show only one line of VAT in the bill.
    # easy method when everything is books.

    # Get the vat of all products, only books or mixed products, be careful.
    # I thought that all books would have the same VAT, but not,
    # we have 978… books with a 20% VAT from Dilicom… go figure! So, no shortcut to take!
    # XXX: small doubt on prices_discounted following cards_data order.

    # TODO: notre "total avant remise" est déjà remisé quand c'est depuis la caisse…
    # We can also add a 4th element to cards_data with the price sold, see above.
    vat_values = Card.get_vat_values(cards_data, prices_discounted=prices_discounted)
    total_vat = sum([it[1] for it in vat_values])

    # On the bill don't show the total if only one line of VAT.
    only_one_vat_type = len(vat_values) == 1

    total_vat_fmt = price_fmt(total_vat, default_currency)

    # Totals
    if show_discount:
        total_label = _("Total before discount")
    else:
        total_label = _("Total incl. taxes")
    total_to_pay_label = _("To pay")

    # Price before taxes (prix HT)
    total_before_taxes = ((total_discounted * 100) - (total_vat * 100)) / 100.0
    total_before_taxes_fmt = price_fmt(total_before_taxes, default_currency)
    total_before_taxes_label = _("Total before taxes")

    # The bill is paid: show "paid" label and change total to pay to 0.
    total_paid_label = _("Already paid")
    total_paid = total
    total_paid_fmt = price_fmt(total_paid, default_currency)
    # if is_paid:
        # total = 0  # feature: allow to give partial payments?
        # total_fmt = price_fmt(total, default_currency)
        # total_discounted_fmt = price_fmt(total, default_currency)

    # Add shipping cost to total with taxes (the total to pay)
    total_to_pay = total_discounted
    if shipping_cost and isinstance(shipping_cost, int):
        shipping_cost_fmt = price_fmt(shipping_cost, default_currency)
        total_to_pay += shipping_cost

    if is_paid:
        total_to_pay_fmt = price_fmt(0, default_currency)
    else:
        total_to_pay_fmt = price_fmt(total_to_pay, default_currency)

    bill_data_dict = {'name': name,
                      'document_title': document_title,
                      'logo_url': Preferences.get_logo_url(),
                      'bon_de_commande': bon_de_commande,
                      'vat_book': vat_book,
                      'vat_values': vat_values,
                      'total_before_taxes_fmt': total_before_taxes_fmt,
                      'total_before_taxes_label': total_before_taxes_label,
                      'total_vat': total_vat,
                      'total_vat_fmt': total_vat_fmt,
                      'total_label': total_label,
                      'total_to_pay': total_to_pay,
                      'total_to_pay_fmt': total_to_pay_fmt,
                      'total_to_pay_label': total_to_pay_label,
                      'total_fmt': total_fmt,
                      'only_one_vat_type': only_one_vat_type,
                      'show_discount': show_discount,
                      'shipping_cost': shipping_cost,
                      'shipping_cost_label': shipping_cost_label,
                      'shipping_cost_fmt': shipping_cost_fmt,
                      'total_discounted_fmt': total_discounted_fmt,
                      'total_with_client_discount': total_with_client_discount,
                      'total_with_client_discount_fmt': price_fmt(total_with_client_discount, default_currency),
                      'show_payment_details': show_payment_details,
                      # Is paid?
                                  'is_paid': is_paid,
                      'total_paid_label': total_paid_label,
                      'total_paid_fmt': total_paid_fmt,

                                  'total_qty': 8,
                      'quantity_header': 18,
                      'creation_date_label': creation_date_label,
                      'creation_date': creation_date,
                      'creation_date_fmt': infile_creation_date_fmt,
                      'discount_label': _("Discount"),
                      'discount_fmt': discount_fmt,
                      'bookshop': bookshop,
                      'client': client,
                      'temporary_client': temporary_client,
                      # books data:
                      'cards_data': cards_data,
                      'prices_sold': prices_sold,
                      'show_discounted_price': show_discounted_price,  # for institutions
    }


    sourceHtml = template.render(bill_data_dict)

    # template2 = get_template(details_template)
    # details_html = template2.render({'cards': cards})

    filepath = os.path.realpath(os.path.join(settings.STATIC_PDF, filename))
    fileurl = "/static/{}".format(filename)
    to_ret = {'fileurl': fileurl,
              'filename': filename,
              'status': 200}

    if with_data_dict:
        # Only for tests, not for API (or must serialize cards_data).
        to_ret['bill_data_dict'] = bill_data_dict  # so we can check them in tests.

    # details_filepath = os.path.realpath(os.path.join(settings.STATIC_PDF, details_filename))
    # details_fileurl = '/static/{}'.format(details_filename)
    # to_ret['details_fileurl'] = details_fileurl
    # to_ret['details_filename'] = details_filename

    try:
        # if target: write to disk, return None.
        if write_pdf:
            with open(filepath, 'wb') as f:
                outhtml = HTML(string=sourceHtml).write_pdf(target=f.name)
        # no target: return the PDF as bytestring.
        else:
            outhtml = HTML(string=sourceHtml).write_pdf()

        to_ret['outhtml'] = outhtml

        # with open(details_filepath, 'wb') as ff:
        #     HTML(string=details_html).write_pdf(target=ff.name)

    except Exception as e:
        log.error("Error writing bill in pdf to {}: {}".format(filepath, e))
        to_ret['status'] = 400

    return to_ret
