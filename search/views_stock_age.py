# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals

import datetime
import io  # write to file in utf8
import json
import os
import time
import traceback
import urllib

import dateparser
import pendulum
import toolz
import unicodecsv
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
# from django.core.paginator import EmptyPage
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import StreamingHttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.template.loader import get_template
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.views.generic import DetailView
from weasyprint import HTML

from abelujo import settings
from search import dilicom_command
from search import forms as viewforms
from search import models

from search.models import Barcode64
from search.models import Basket
from search.models import InstitutionBasket
from search.models import Bill
from search.models import Card
from search.models import CardType
from search.models import Client
from search.models import Command
from search.models import Distributor
from search.models import Entry
from search.models import EntryCopies
from search.models import Inventory
from search.models import InventoryCommand
from search.models import Place
from search.models import Preferences
from search.models import Publisher
from search.models import Sell
from search.models import SoldCards
from search.models import Shelf
from search.models import Stats
from search.models import history
from search.models import users
from search.models import bill_utils

from search.models.api import _get_command_or_return
from search.models.common import ALERT_ERROR
from search.models.common import ALERT_SUCCESS
from search.models.common import ALERT_WARNING
from search.models.common import get_payment_abbr
from search.models.utils import _is_falsy
from search.models.utils import _is_truthy
from search.models.utils import get_logger
from search.models.utils import get_total_weight
from search.models.utils import is_isbn
from search.models.utils import ppcard
from search.models.utils import price_fmt
from search.models.utils import roundfloat
from search.models.utils import truncate

from views_utils import DEFAULT_DATASOURCE
from views_utils import Echo
from views_utils import bulk_import_from_dilicom
from views_utils import cards2csv
from views_utils import dilicom_enabled
from views_utils import electre_enabled
from views_utils import extract_all_isbns_quantities
from views_utils import format_price_for_locale
from views_utils import is_official_datasource
from views_utils import is_htmx_request
from views_utils import update_from_dilicom_with_cache
from views_utils import update_from_electre_with_cache

log = get_logger()

DEFAULT_NB_COPIES = 1         # default nb of copies to add.

PENDULUM_YMD = '%Y-%m-%d'  # caution, %m is a bit different than datetime's %M.


@login_required
def shelf_age(request, *args, **kw):
    """
    Get the cards of the given age: 0-3 months, 3-6… >24.
    """
    template = "search/shelf_age.html"
    if is_htmx_request(request.META):
        template = "search/shelf_age_content.html"

    if request.method == 'GET':
        params = request.GET.copy()
        pk = params.get('shelf_id', 1)
        age = int(params.get('age', 1))  # not gonna accept weird user input.
        page = int(params.get('page', 1))

        data = Stats.shelf_age_cards(pk, age, page=page)

        return render(request, template, {
            'cards': data['data'],
            'meta': data['meta'],
        })
