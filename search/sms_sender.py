# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Send an SMS to a client.

We can use a custom sender name.
https://www.twilio.com/docs/sms/send-messages#use-an-alphanumeric-sender-id
> Alphanumeric sender IDs are used for branded one-way messaging. Instead of using an E.164 formatted Twilio phone number for the "From" value, you can use a custom string like your own business' branding. Alphanumeric sender IDs may be used at no additional cost when sending an SMS to countries that support this feature.

"""

from __future__ import unicode_literals

from search.models.utils import Messages
import logging
from search.models.utils import get_logger

from abelujo import settings

try:
    from twilio.rest import Client
except ImportError:
    print("twilio is not available")

logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s:%(lineno)s]:%(message)s', level=logging.DEBUG)
log = get_logger()


# A global SMS client. With the right tokens, it shoul be necessary to instantiate it
# only once.
CLIENT = None

#: The SMS sender name, that will appear in the client's phone.
ALPHANUM_SENDER_ID = "Librairie"


def is_valid_sender_name(txt):
    if not txt:
        log.warning("The SMS sender name '{}' is void.".format(txt))
        return False
    if not txt.isalnum():
        log.warning("The SMS sender name is not alpha num: {}".format(txt))
        return False
    if not len(txt) <= 11:
        log.warning("The SMS sender name '{}' must be up to 11 characters long.".format(txt))
        return False
    return True


# Run at startup to see the logs:
is_valid_sender_name(settings.TWILIO_SENDER_NAME)


def get_sms_account():
    return settings.TWILIO_ACCOUNT

def get_sms_token():
    return settings.TWILIO_TOKEN

def get_sms_sender():
    if settings.TWILIO_SENDER_NAME and is_valid_sender_name(settings.TWILIO_SENDER_NAME):
        return settings.TWILIO_SENDER_NAME
    elif ALPHANUM_SENDER_ID:
        return ALPHANUM_SENDER_ID
    # Otherwise, the client will see "SMS" (Twilio default).
    return settings.TWILIO_SENDER

def get_sms_client():
    """
    If the Client object is not set, set it.

    Return: a Client object (by the SMS library, like Twilio).
    """
    global CLIENT
    if CLIENT is None:
        CLIENT = Client(get_sms_account(), get_sms_token())

    return CLIENT

def send_sms(body="",
             receiver="",
             sender=None,
             client=None):
    """
    Send the SMS.

    - sender: if None, get our. String .
    - receiver: same. (tel number starting with +336…)
    - body: string.

    Return: a tuple status, Messages() object.
    """
    msgs = Messages()
    twilio_client = get_sms_client()
    sms_message = None
    sms_sent = None
    if not sender:
        sender = get_sms_sender()
    try:
        sms_message = twilio_client.messages.create(to=receiver,
                                                    from_=sender,
                                                    body=body)
    except Exception as e:
        log.error("Error sending SMS to {}: {}".format(receiver, e))
        msgs.add_error("Sending the SMS failed")
        sms_sent = False

    # Here the SMS status is "queued".
    if sms_message is not None:
        if sms_message.error_code is None:
            sms_sent = True

    if sms_sent or True:
        # Save for client.
        if client:
            sms_obj = client.save_sms(body)
            if not sms_obj:
                log.warning("Error saving the SMS sent to {}. Returned SMS object is null".format(client))

    return msgs.status, msgs
