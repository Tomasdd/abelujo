# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
#
# We handle here: lists, boxes, returns.
#
# variables:
# baskets: contains the list of baskets to show.
# cur_basket: the selected basket. Show its books.
# cur_basket.copies: the books to show.
#
# Are we in a regular list, a box, a return?
# - use boxes_page and returns_page
#
#
# Lists
# - a basket object per list on the backend.
#
# Returns
# - a basket object per supplier on the backend.
# - we handle one default list that dispatches to the supplier of the added book.
#
##################################################################

angular.module "abelujo" .controller 'basketsController', ['$http', '$scope', '$timeout', '$filter', '$window', '$uibModal', '$log', 'utils', '$location', 'hotkeys', ($http, $scope, $timeout, $filter, $window, $uibModal, $log, utils, $location, hotkeys) !->

    {Obj, join, reject, sum, map, filter, find, lines, sort-by, find-index, reverse} = require 'prelude-ls'

    $scope.baskets = []  # the list of baskets to show on this page.
    # for Returns, we can scan an ISBN to the "default basket",
    # so that the book goes to its correct supplier basket automatically.
    $scope.returns_default_basket = {id: -99, copies: []}
    $scope.copies = []  # The ones to display.
    $scope.alerts = []
    $scope.show_buttons = {}
    $scope.new_name = null
    $scope.cur_basket = undefined
    $scope.cards_fetched = [] # fetched from the autocomplete
    $scope.copy_selected = undefined
    $scope.show_images = false
    $scope.total_weight = undefined

    # Additional fields for the Bill modale:
    $scope.bon_de_commande_id = ""
    $scope.shipping_cost = undefined

    $scope.selected_client = null  # in a modale.

    COMMAND_BASKET_ID = 1

    $scope.language = utils.url_language($window.location.pathname)

    # Are we on the /boxes page?
    url_match = $window.location.pathname.match("\/boxes")
    if url_match and url_match is not null
      $scope.boxes_page = true
      $window.document.title = "Abelujo - " + gettext("Boxes")
    else
      $scope.boxes_page = false

    $window.document.title = "Abelujo - " + gettext("Baskets")

    # Are we on the /returns page?
    url_match = $window.location.pathname.match("\/returns")
    if url_match and url_match is not null
      $scope.returns_page = true
      $window.document.title = "Abelujo - " + gettext("Returns")
    else
      $scope.returns_page = false

    # Are we on /archives ?
    # We wouldn't need this mess with proper inheritance of a templating system! (not Jade)
    $scope.archives_page = false
    url_match = $window.location.pathname.match("\/archives")
    document.getElementById('baskets_tab_id').classList.add('active')
    if url_match and url_match is not null
      # Archives tab.
      $scope.archives_page = true
      # change active tab.
      document.getElementById('baskets_tab_id').classList.remove('active')
      document.getElementById('archives_tab_id').classList.add('active')
      ## document.getElementById('archives_tab_id').style.background-color = "red"
      # Disable Actions button.
      document.getElementById('action-button-id').disabled = true
      # Disable cards input.
      document.getElementById('default-input').disabled = true
      # Disable all remove buttons.
      ## remove_buttons = document.getElementsByClassName('my_remove')
      ## for button in remove_buttons
        ## button.style.display = "none"
      $window.document.title = "Abelujo - " + gettext("Baskets") + "(" + gettext("Archives") + ")"

    else
      # Normal Lists tab.
      # hide "archived on".
      document.getElementById('archived_on_id').style.display = "none"

    # Center notifications.
    Notiflix.Notify.Init do
        timeout: 7000
        messageMaxLength: 220
        position: "left-top"

    $scope.showing_notes = false
    $scope.last_sort = "title"

    # pagination
    $scope.page = 1
    $scope.page_size = 100
    $scope.page_sizes = [25, 50, 100, 200, 500]
    $scope.page_max = 1
    $scope.meta = do
        num_pages: null
        nb_results: null
        total_weight: 0
    page_size = $window.localStorage.getItem "baskets_page_size"
    if page_size != null
        $scope.page_size = parseInt(page_size)

    params = {}
    if $scope.boxes_page
       params['boxes'] = true  # beware, we can have a list with 'false'
    if $scope.returns_page
       params['supplier_returns'] = true  # beware, we can have a list with 'false'
    if $scope.archives_page
       params['archives'] = true  # beware, we can have a list with 'false'

    $http.get "/api/baskets", do
      params: params
    .then (response) ->
        """Get the baskets, do not show the "to command" one, of id=1.
        """
        $scope.baskets = response.data.data
        |> reject (.id == 1)

        # Returns
        if $scope.returns_page and $location.hash() in ["default", "#", "##", ""]
            $scope.cur_basket_index = -99
            $scope.showBasket -99
            return

        hash_basket_id = parseInt $location.hash(), 10

        index = find-index ( -> hash_basket_id == it.id), $scope.baskets
        if not index
            index = 0

        $scope.cur_basket_index = index
        $scope.showBasket index

    $scope.save_basket = !->
        params = do
            comment: $scope.cur_basket.comment

        $log.info "updating basket #{$scope.cur_basket.id}…"
        $http.post "/api/baskets/#{$scope.cur_basket.id}/update/", params
        .then (response) ->
            resp = response.data

    $scope.getCopies = (index) !->
        if $scope.cur_basket
            params = do
                page_size: $scope.page_size
                page: $scope.page
            $http.get "/api/baskets/#{$scope.cur_basket.id}/copies", do
                params: params
            .then (response) !->
                $scope.baskets[index].copies = response.data.data
                $scope.meta = response.data.meta
                $scope.copies = response.data.data
                $scope.total_weight = $scope.meta.total_weight

    $scope.set_window_title_with_basket = (name) !->
        title = "Abelujo - "
        if $scope.returns_page
            title += gettext("Returns")
        else if $scope.boxes_page
            title += gettext("Boxes")
        else
            title += gettext("Baskets")
        title += ", " + name
        $window.document.title = title

    $scope.showBasket = (index) !->
        "Show the copies of the given basket."
        # For Returns, we can use a default basket
        # so we don't have to select a Supplier on the side.
        if index == -99
          $scope.cur_basket = $scope.returns_default_basket
          $scope.copies = $scope.returns_default_basket.copies
          $location.hash("default")
          utils.set_focus!
          return

        $scope.cur_basket = $scope.baskets[index]

        if $scope.cur_basket
            $location.hash($scope.cur_basket.id)
            $scope.set_window_title_with_basket $scope.cur_basket.name
            # For the choose-shelf modale. We didn't find how to pass a custom parameter to it.
            $window.localStorage.setItem('cur_basket_id', $scope.cur_basket.id)

            if not $scope.cur_basket.copies
                $scope.getCopies index
            else
                $scope.copies = $scope.cur_basket.copies

        # Set focus.
        angular.element('#default-input').trigger('focus')
        ## $scope.get_total_weight!

    $scope.empty_basket = !->
        sure = confirm(gettext("Are you sure to empty this list {}?")replace "{}", $scope.cur_basket.name)
        if sure
            $http.post "/api/baskets/#{$scope.cur_basket.id}/empty"
            .then (response) !->
                index = find-index (.id == $scope.cur_basket.id), $scope.baskets
                $scope.baskets[index].copies = []
                $scope.baskets[index].length = 0
                $scope.copies = []

    $scope.do_archive_basket = !->
        $http.post "/api/baskets/#{$scope.cur_basket.id}/archive"
        .then (response) !->
            index = find-index (.id == $scope.cur_basket.id), $scope.baskets
            $scope.baskets.splice index, 1
            $scope.showBasket index

    $scope.archive_basket =  !->
        sure = confirm(gettext("Are you sure to archive this list {}?")replace "{}", $scope.cur_basket.name)
        if sure
           $scope.do_archive_basket!

    $scope.delete_basket =  !->
        # deprecated, removed from UI nov, 2021.
        sure = confirm(gettext("You are going to delete the list {}. This can not be undone. Are you sure ?")replace "{}", $scope.cur_basket.name)
        if sure
            $http.post "/api/baskets/#{$scope.cur_basket.id}/delete"
            .then (response) !->
                index = find-index (.id == $scope.cur_basket.id), $scope.baskets
                $scope.baskets.splice index, 1
                if index >= $scope.baskets.length
                    index -= 1
                $scope.showBasket index

    $scope.closeAlert = (index) ->
        $scope.alerts.splice index, 1

    $scope.getCards = (query) ->
        args = do
            query: query
            language: $scope.language
            lang: $scope.language
            with_return_quantity: true  # for Returns, using the default basket.

        promise = utils.getCards args
        # getCards only returns the list of cards,
        # we don't get the res.meta data (nb_results, page…).
        if promise
            promise.then (res) ->
                $scope.cards_fetched = res

                # Searched an ISBN, found it:
                if utils.is_isbn(query) and res.length == 1
                   setTimeout( ->
                     $window.document.getElementById("default-input").value = ""
                     $scope.add_selected_card res[0]
                   , 700)
                   return

                # Searched an ISBN, no results:
                if utils.is_isbn(query) and res.length == 0
                   Notiflix.Notify.Merge({
                     timeout: 20000,
                   })
                   Notiflix.Notify.Warning(gettext('ISBN not found.'))
                   Notiflix.Notify.Info gettext('You can still create a card manually.')
                   $window.document.getElementById("default-input").value = "";
                   return

                return res

    $scope.add_selected_card = (card) !->
        """ Add the card selected from the autocomplete to the current list's copies.
        Save it.
        """
        # tmpcard: the card we are adding to the current view/basket.
        # existing: if it is already present in the current view. We update its basket_qty.
        #
        # When we search for cards, we don't know how many there is in the current basket.
        # Maybe we could know, because a basket is selected.
        # But with Returns, and with the "default" basket, we don't have to select a basket,
        # the card will be added to the Return basket of its distributor.
        #
        # That's why the basket_qty is updated on the client side.
        # Could maybe do better.
        #
        # If the card exists in the current list, move it at the top.

        now = luxon.DateTime.local().toFormat('yyyy-LL-dd HH:mm:ss')
        tmpcard = $scope.cards_fetched
        |> find (.id == card.id)

        if not tmpcard
           $log.warn "we were expecting an existing tmpcard amongst cards_fetched ", $scope.cards_fetched
           Notiflix.Notify.Warning "Le serveur n'est pas prêt ! Veuillez attendre un instant et ré-essayer, merci."
           $scope.copy_selected = undefined
           return

        tmpcard = tmpcard.item

        $log.info "tmpcard qty in basket. Is it up to date? No. The search doesn't know the basket… ", tmpcard.basket_qty, tmpcard, tmpcard.return_qty  # return_qty is a new data.

        # $scope.copies.push tmpcard
        ## # Insert at the right sorted place
        ## index = 0
        ## index = find-index ( -> tmpcard.title < it.title), $scope.copies
        ## if not index
        ##     index = $scope.copies.length
        ## $scope.copies.splice index, 0, tmpcard

        # Get possibly existing card and move to the top of the list.
        # Normal case.
        # Find the copy in the current basket list.
        existing = $scope.copies
        |> find (.id == tmpcard.id)

        #
        # Returns
        #
        # If it's a return using the default list,
        # we should update the card's basket_qty of the real list,
        # and update the quantity in this default list too.
        #
        # Status 2023-01-24:
        # - we get the exact quantity from the server, so after a scan,
        # the quantity is right.
        # - when we scan in the default list, the quantity is correctly updated to
        # the other basket of the supplier.
        # - when we go to this supplier basket, scan a book, and go back to the default
        # list, the quantity was not updated.
        # But, if we scan again, we get the right quantity.
        #

        # We return a book and we scan it in the default list.
        # So, find the basket for this distributor.
        if $scope.returns_page

           if not tmpcard.distributor.id
              Notiflix.Notify.Warning "No distributor for this card"

           basket_for_supplier_index = find-index ( -> tmpcard.distributor.id == it.distributor_id ), $scope.baskets
           $log.info "basket pour distributeur (index)", basket_for_supplier_index
           basket_for_supplier = $scope.baskets[basket_for_supplier_index]
           $log.info basket_for_supplier
           # Does this card exist there? Then update. If not, add.
           # We want to see the card in both lists… the default, the real list.
           existing_in_other_basket = undefined

           if basket_for_supplier.copies
              # ah oui mais là on ne saura pas la quantité dans le basket
              # qui n'a pas encore été récupéré…
              # C'est le backend qui devrait nous donner la qty.
              # #TODO:
              existing_in_other_basket = basket_for_supplier.copies
              |> find (.id == tmpcard.id)
              if existing_in_other_basket and $scope.cur_basket.id == -99
                 existing_in_other_basket.basket_qty = tmpcard.return_qty
                 existing_in_other_basket.basket_qty += 1
                 ## else
                 ##    $log.info "unshift ?"
                 ##    basket_for_supplier.copies.unshift existing_in_other_basket
                 ## basket_for_supplier.copies += tmpcard.id
                 # existing card in basket copies?

        #
        # Back to normal. We update the card of the current view/basket.
        #
        if existing
           # Update quantity in basket to show.
           if $scope.returns_page and existing_in_other_basket and existing_in_other_basket.basket_qty  and $scope.cur_basket.id != -99
              existing_in_other_basket.basket_qty = tmpcard.return_qty
              existing_in_other_basket.basket_qty += 1

           # Normal case:
           if $scope.returns_page
              existing.basket_qty = tmpcard.return_qty
           existing.basket_qty += 1  # and move to top

           existing.modified = now
           index = find-index ( -> existing.id == it.id ), $scope.copies
           if index
               # remove existing from list
               $scope.copies.splice index, 1
               # and move to top.
               $scope.copies.unshift existing
        else
           tmpcard.modified = now
           # Returns: update this basket_qty for the default view.
           # return_qty is the true value returned by the server.
           # It's new, we could use it in more places
           # (although the previous behaviour works (in best-case scenarios)).
           if $scope.returns_page
              tmpcard.basket_qty = tmpcard.return_qty
              tmpcard.basket_qty += 1

           # Normal:
           $scope.copies.unshift tmpcard

        $scope.copy_selected = undefined
        # TODO: save and handle errors.
        if $scope.cur_basket.id == -99 and $scope.returns_page
           $scope.save_and_return_card_to_its_supplier tmpcard.id
        else
           $scope.save_card_to_basket tmpcard.id, $scope.cur_basket.id

    $scope.save_card_to_basket = (card_id, basket_id) !->
        # XXX seems redundant with save_quantity below
        coma_sep = "#{card_id}"
        params = do
            card_ids: coma_sep
        $http.post "/api/baskets/#{basket_id}/add/", params
        .then (response) !->
            $log.info "added cards to basket"
            # $scope.alerts = response.data.msgs # the confirmation alert should be less intrusive
            ## $scope.get_total_weight!
            Notiflix.Notify.Success "OK"

            # Check we have the same distributor. If not, show some red.
            if $scope.cur_basket.distributor
               existing = $scope.copies
               |> find (.id == card_id)
               if existing.distributor and existing.distributor.name != $scope.cur_basket.distributor
                  $log.warn "Not the same distributor !"
                  Notiflix.Notify.Warning gettext "Not the same distributor !"
                  elt = document.getElementById("suppliercard" + card_id)
                  elt.style.backgroundColor = "orange"
                  basket_supplier = document.getElementById("supplier")
                  basket_supplier.style.backgroundColor = "orange"

        , (response) !->
            Notiflix.Notify.Warning "Something went wrong."
            ... # error

    # Save this card to the basket of its supplier (in case of a Return).
    $scope.save_and_return_card_to_its_supplier = (card_id) !->
        # #TODO: increment the nb of the real basket.
        params = do
            card_id: card_id
        $http.post "/api/returns/add", params
        .then (response) !->
            $log.info response

            Notiflix.Notify.Success "OK"

        , (response) !->
            Notiflix.Notify.Warning "Something went wrong."
            ... # error

    $scope.save_quantity = (index) !->
        """
        Save the item quantity.

        In case of a box / supplier return, the quantity is decremented (or incremented) according to the difference with the previous quantity in this basket.

        Called on a change event of the quantity input field.
        """
        # XXX see save_card_to_basket above
        card = $scope.copies[index]
        now = luxon.DateTime.local().toFormat('yyyy-LL-dd HH:mm:ss')
        utils.save_quantity card, $scope.cur_basket.id
        card.modified = now  # there is no success check
        ## $scope.get_total_weight!

    $scope.command = !->
        """Add the copies of the current basket to the Command basket. Api call.
        """
        if not confirm gettext "You didn't set a supplier for this list (menu -> set a supplier). Do you want to carry on ?"
           return

        if not $scope.copies.length
            alert gettext "This basket has no copies to command !"
            return

        text = gettext("Do you want to mark all the cards of this list to command ?")
        if $scope.cur_basket.distributor
            text += gettext " They will be associated with the supplier #{$scope.cur_basket.distributor}."
        sure = confirm text
        if sure
            # We command all the basket.
            params = do
                basket_id: $scope.cur_basket.id
            $http.post "/api/baskets/#{COMMAND_BASKET_ID}/add/", params
            .then (response) !->
                # $scope.alerts = response.data.msgs
                $scope.alerts = response.data.alerts
                if response.data.status == "success"
                  Notiflix.Notify.Success "OK"

    $scope.remove_from_selection = (index_to_rm) !->
        "Remove the card from the list. Server call to the api."
        card_id = $scope.copies[index_to_rm].id
        utils.set_focus!
        params = {}
        if $scope.boxes_page or $scope.returns_page
           params['is_box'] = true
        $http.post "/api/baskets/#{$scope.cur_basket.id}/remove/#{card_id}/", params
        .then (response) !->
            $scope.copies.splice(index_to_rm, 1)
            # $scope.alerts = response.data.msgs # useless
            ## $scope.get_total_weight!

        .catch (resp) !->
            $log.info "Error when trying to remove the card " + card_id

    $scope.get_data = ->
        # coma-sep list of ids:
        $scope.cur_basket.copies
        |> map (.id)
        |> join ","

    $scope.get_total_price = ->
        utils.total_price $scope.copies

    $scope.get_total_copies = ->
        utils.total_copies $scope.copies

    $scope.get_total_weight = !->
        # We got the original total weight from the API (in case the basket is large)
        # and we now sum or substract the weight of books we add or remove.
        #XXX: not ready. Updates stupidely.
        $scope.total_weight = $scope.copies
        |> map ( -> it.weight * it.quantity )
        |> sum

    $scope.receive_command = !->
        if not $scope.cur_basket.distributor
           alert "You didn't set a distributor for this basket. Please see the menu Action -> set the supplier."
           return
        sure = confirm gettext "Do you want to receive a command for the supplier '#{$scope.cur_basket.distributor}' ?"
        if sure
            # Get or create the inventory, redirect to that inventory page
            $http.get "/api/baskets/#{$scope.cur_basket.id}/inventories/"
            .then (response) !->
                inv_id = response.data.data.inv_id
                # What url scheme ? We want to include the inv id to re-use the inventory controller.
                # baskets/<basket_id>/inventory/<inv id>/, or
                #  /inventories/<inv id>
                # $window.location.href = "/#{$scope.language}/baskets/#{$scope.cur_basket.id}/receive/"
                $window.location.href = "/#{$scope.language}/inventories/#{inv_id}/"

    $scope.return_to_supplier = !->
        if not $scope.cur_basket.distributor
           alert "You didn't set a distributor for this basket. Please see the menu Action -> set the supplier."
           return
        sure = confirm gettext "Do you want to return this basket to #{$scope.cur_basket.distributor} ? This will remove the given quantities from your stock."
        if sure
            $http.post "/api/baskets/#{$scope.cur_basket.id}/return"
            .then (response) !->
                $scope.alerts = response.data.alerts
                # if response.data.status == "success" …

    #########################################
    ## Pagination
    #########################################
    $scope.$watch "page_size", !->
        $window.localStorage.baskets_page_size = $scope.page_size
        $scope.getCopies $scope.cur_basket_index

    $scope.nextPage = !->
        if $scope.page < $scope.meta.num_pages
            $scope.page += 1
            $scope.getCopies $scope.cur_basket_index

    $scope.lastPage = !->
        $scope.page = $scope.meta.num_pages
        $scope.getCopies $scope.cur_basket_index

    $scope.previousPage = !->
        if $scope.page > 1
            $scope.page -= 1
            $scope.getCopies $scope.cur_basket_index

    $scope.firstPage =!->
        $scope.page = 1
        $scope.getCopies $scope.cur_basket_index


    #############################
    # Open new basket modal
    # ###########################
    $scope.open_new_basket = (size) !->
        # angular.element('#modal-input').trigger('focus')  # noop :(
        modalInstance = $uibModal.open do
            animation: $scope.animationsEnabled
            templateUrl: 'basketModal.html'
            controller: 'BasketModalControllerInstance'
            ## backdrop: 'static'
            size: size,
            resolve: do
                utils: ->
                    utils

        modalInstance.result.then (basket) !->
            $scope.baskets.unshift basket
            $log.info "new basket: ", basket
            $scope.showBasket 1
            $scope.cur_basket = basket
        , !->
            $log.info "modal dismissed"

    #############################
    # Choose and add to shelf modal
    # ###########################
    ## $scope.add_to_shelf = (size) !->
    $scope.add_to_shelf = (cur_basket_id) !->
        modalInstance = $uibModal.open do
            animation: $scope.animationsEnabled
            templateUrl: 'chooseShelfModal.html'
            controller: 'ChooseShelfModalControllerInstance'
            ## backdrop: 'static'
            ## size: size,
            cur_basket_id: cur_basket_id,
            resolve: do
                utils: ->
                    utils

        modalInstance.result.then () !->
            # Empty the basket (double work from the api).
            basket = $scope.baskets[cur_basket_id]
            basket.copies = []
            $scope.copies = []
        , !->
            $log.info "modal dismissed"

    ##################################################################
    # Change the shelf.
    ##################################################################
    $scope.change_shelf = (cur_basket_id) !->
        modalInstance = $uibModal.open do
            animation: $scope.animationsEnabled
            templateUrl: 'changeShelfModal.html'
            controller: 'ChangeShelfModalControllerInstance'
            ## backdrop: 'static'
            ## size: size,
            cur_basket_id: cur_basket_id,
            resolve: do
                utils: ->
                    utils

        modalInstance.result.then () !->
            # Empty the basket (double work from the api).
            basket = $scope.baskets[cur_basket_id]
            basket.copies = []
            $scope.copies = []
        , !->
            $log.info "modal dismissed"


    ##################################################################
    # Add all cards to the stock, straight, without choosing the shelf.
    ###################################################################
    $scope.add_to_stock = (cur_basket_id) !->
        params = do
            shelf_id: cur_basket_id

        sure = confirm gettext "Do you want to add all the cards to your stock?"
        if sure
            $http.post "/api/baskets/#{cur_basket_id}/add_to_stock/", params
            .then (response) !->
                if response.data.status == "success"
                   ## Notiflix.Notify.Success "OK"
                   $scope.alerts = response.data.alerts

                   # Remove basket from UI.
                   index = find-index (.id == cur_basket_id), $scope.baskets
                   $scope.baskets.splice index, 1
                   if index >= $scope.baskets.length
                       index -= 1
                   $scope.showBasket index

                else
                   Notiflix.Notify.Info "Warning: it seems that an error occured."

    $scope.toggle_images = !->
        $scope.show_images = not $scope.show_images

    $scope.sort_by = (key) !->
        if $scope.last_sort == key
            $scope.copies = $scope.copies
            |> reverse
        else
            $scope.copies = $scope.copies
            |> sort-by ( -> it[key])
            $scope.last_sort = key

        $log.info $scope.copies

    #############################
    # Choose a client for billing
    # ###########################
    $scope.choose_client_for_bill = (cur_basket_id, bill_or_estimate) !->
        "1: bill, 2: estimate, 3: bill for a sell 'facture de caisse'."
        # pass this value to the modal controller:
        $window.localStorage.setItem('checkboxsell', true)
        $window.localStorage.setItem('bill_or_estimate', bill_or_estimate)

        modalInstance = $uibModal.open do
            animation: $scope.animationsEnabled
            templateUrl: 'chooseClientModal.html'
            controller: 'ChooseClientModalControllerInstance'
            ## backdrop: 'static'
            ## size: size,
            cur_basket_id: cur_basket_id,
            bill_or_estimate: bill_or_estimate,
            resolve: do
                utils: ->
                    utils

    #############################
    # Choose a client to sell
    # ###########################
    $scope.choose_client_to_sell = (cur_basket_id) !->
        if not $scope.copies.length
            Notiflix.Notify.Info "Cette liste est vide"
            return

        if $scope.cur_basket.sold_date
            sure = confirm(gettext("Cette liste a déjà été vendue le #{$scope.cur_basket.sold_date }, continuer ?"))
            if not sure
               return

        # pass this value to the modal controller:
        modalInstance = $uibModal.open do
            animation: $scope.animationsEnabled
            templateUrl: 'chooseClientToSellModal.html'
            controller: 'ChooseClientToSellModalControllerInstance'
            ## backdrop: 'static'
            ## size: size,
            cur_basket_id: cur_basket_id,
            resolve: do
                utils: ->
                    utils

    ################################
    # Choose a client for an estimate (no sell)
    # ##############################
    $scope.choose_client_for_estimate = (cur_basket_id) !->
        "1: bill, 2: estimate"
        # pass this value to the modal controller:
        $window.localStorage.setItem('checkboxsell', false)
        $window.localStorage.setItem('bill_or_estimate', "2")  # 2 = estimate

        modalInstance = $uibModal.open do
            animation: $scope.animationsEnabled
            templateUrl: 'chooseClientForEstimateModal.html'
            controller: 'ChooseClientModalControllerInstance'
            ## backdrop: 'static'
            ## size: size,
            cur_basket_id: cur_basket_id,
            resolve: do
                utils: ->
                    utils

    # Listen for some HTMX backend events.
    # Currently done in client_institution_basket.html.

    ##############################
    # Keyboard shortcuts (hotkeys)
    # ############################
    hotkeys.bindTo($scope)
    .add do
        combo: "d"
        description: gettext "show or hide the book details in tables."
        callback: !->
            $scope.toggle_images!

    .add do
        combo: "s"
        description: gettext "go to the search box"
        callback: !->
           utils.set_focus!

    .add do
        combo: "n"
        description: gettext "hide or show your notes"
        callback: !->
            $scope.showing_notes = ! $scope.showing_notes

    .add do
        combo: "C"
        description: gettext "Create a new list"
        callback: !->
            $scope.open!

]

#####################
# New basket modal
# ###################
angular.module "abelujo" .controller "BasketModalControllerInstance", ($http, $scope, $uibModalInstance, $window, $log, utils) !->

    utils.set_focus!

    $scope.ok = !->
        if typeof ($scope.new_name) == "undefined" || $scope.new_name == ""
            $uibModalInstance.dismiss('cancel')
            return

        params = do
            name: $scope.new_name

        if $window.location.pathname.match "\/boxes"
          params['box'] = true

        $http.post "/api/baskets/create", params
        .then (response) !->
            basket = response.data.data
            $scope.alerts = response.data.alerts
            $uibModalInstance.close(basket)

    $scope.cancel = !->
        $uibModalInstance.dismiss('cancel')

#####################
# Choose/add to shelf modal
# ###################
angular.module "abelujo" .controller "ChooseShelfModalControllerInstance", ($http, $scope, $uibModalInstance, $window, $log, utils) !->

    utils.set_focus!
    $scope.shelves = []
    $scope.selected_shelf = null

    $scope.cur_basket_id = $window.localStorage.getItem('cur_basket_id')

    $http.get "/api/shelfs"
    .then (response) ->
        $scope.shelves = response.data

    $scope.ok = !->
        if typeof ($scope.selected_shelf) == "undefined" || $scope.selected_shelf == ""
            $uibModalInstance.dismiss('cancel')
            return

        #  This is needed for Django to process the params to its
        #  request.POST dictionnary:
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

        #  We need not to pass the parameters encoded as json to Django.
        #  Encode them like url parameters.
        $http.defaults.transformRequest = utils.transformRequestAsFormPost # don't transfrom params to json.
        config = do
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        params = do
            shelf_id: $scope.selected_shelf.pk

        $http.post "/api/baskets/#{$scope.cur_basket_id}/add_to_shelf/", params
        .then (response) !->
            $scope.alerts = response.data.alerts
            $uibModalInstance.close()
            $scope.alerts = response.data.alerts

    $scope.cancel = !->
        $uibModalInstance.dismiss('cancel')
        ## $scope.alerts = response.data.alerts

#####################
# Change shelf modal
# (mostly copied from ChooseShelfModalControllerInstance above)
# ###################
angular.module "abelujo" .controller "ChangeShelfModalControllerInstance", ($http, $scope, $uibModalInstance, $window, $log, utils) !->

    utils.set_focus!
    $scope.shelves = []
    $scope.selected_shelf = null

    $scope.cur_basket_id = $window.localStorage.getItem('cur_basket_id')

    $http.get "/api/shelfs"
    .then (response) ->
        $scope.shelves = response.data

    $scope.ok = !->
        if typeof ($scope.selected_shelf) == "undefined" || $scope.selected_shelf == ""
            $uibModalInstance.dismiss('cancel')
            return

        #  This is needed for Django to process the params to its
        #  request.POST dictionnary:
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

        #  We need not to pass the parameters encoded as json to Django.
        #  Encode them like url parameters.
        $http.defaults.transformRequest = utils.transformRequestAsFormPost # don't transfrom params to json.
        config = do
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        params = do
            shelf_id: $scope.selected_shelf.pk

        $http.post "/api/baskets/#{$scope.cur_basket_id}/change_shelf/", params
        .then (response) !->
            $scope.alerts = response.data.alerts
            $uibModalInstance.close()
            $scope.alerts = response.data.alerts
            if (response.status == 200)
               Notiflix.Notify.Success "OK"
        .catch (resp) !->
            $log.info "Error when trying to change the shelf"
            Notiflix.Notify.Warning "mmh…"

    $scope.cancel = !->
        $uibModalInstance.dismiss('cancel')
        ## $scope.alerts = response.data.alerts

###############################
# Choose a client (for billing)
# #############################
angular.module "abelujo" .controller "ChooseClientModalControllerInstance", ($http, $scope, $uibModalInstance, $window, $log, utils) !->

    utils.set_focus!
    $scope.shelves = []
    $scope.checkboxsell = $window.localStorage.getItem("checkboxsell")    # if we ask a bill in a basket, we probably want to sell the books.

    $scope.cur_basket_id = $window.localStorage.getItem('cur_basket_id')

    $http.get "/api/clients"
    .then (response) ->
        $scope.clients = response.data.data

    $scope.ok = !->
        if typeof ($scope.selected_client) == "undefined" || $scope.selected_client == ""
            $uibModalInstance.dismiss('cancel')
            Notiflix.Notify.Info gettext "You didn't select a client."
            return

        checkboxsell = $scope.checkboxsell
        bill_or_estimate = $window.localStorage.getItem('bill_or_estimate')
        if bill_or_estimate == 1
          checkboxsell = false

        params = do
            client_id: $scope.selected_client.id
            basket_id: $scope.cur_basket_id
            language: utils.url_language($window.location.pathname)
            bill_or_estimate: bill_or_estimate
            checkboxsell: checkboxsell
            bon_de_commande_id: $scope.bon_de_commande_id
            shipping_cost: $scope.shipping_cost

        # api_users
        $http.post "/api/bill", params
        .then (response) !->
            $scope.alerts = response.data.alerts
            $uibModalInstance.close()
            $scope.alerts = response.data.alerts
            if (response.status == 200)
                    element = document.createElement('a')
                    element.setAttribute('href', response.data.fileurl)
                    element.setAttribute('download', response.data.filename)
                    element.style.display = 'none'
                    document.body.appendChild(element)
                    element.click()
                    document.body.removeChild(element)


    $scope.cancel = !->
        $uibModalInstance.dismiss('cancel')
        ## $scope.alerts = response.data.alerts

###############################
# Choose a client (to Sell)
# #############################
angular.module "abelujo" .controller "ChooseClientToSellModalControllerInstance", ($http, $scope, $uibModalInstance, $window, $log, utils) !->

    {Obj, join, reject, sum, map, filter, find, lines, sort-by, find-index, reverse} = require 'prelude-ls'

    utils.set_focus!
    $scope.shelves = []
    $scope.payment_means = []

    $scope.cur_basket_id = $window.localStorage.getItem('cur_basket_id')

    $http.get "/api/clients"
    .then (response) ->
        $scope.clients = response.data.data

    $http.get("/api/config/payment_choices")
    .then (response) ->
        $scope.payment_means = response.data.data

    $scope.ok = (payment_id) ->
        $log.info "payment: ", payment_id

        if typeof ($scope.selected_client) == "undefined" || $scope.selected_client == ""
            $uibModalInstance.dismiss('cancel')
            Notiflix.Notify.Info gettext "You didn't select a client."
            return

        copies = $scope.cur_basket_id
        params = do
            client_id: $scope.selected_client.id
            payment_id: payment_id
            language: utils.url_language($window.location.pathname)

        $http.post "/api/baskets/#{$scope.cur_basket_id}/sell", params
        .then (response) !->
            $scope.alerts = response.data.alerts
            $uibModalInstance.close()
            $scope.alerts = response.data.alerts
            if (response.status !== 200)
               Notiflix.Notify.Info("The sell got an error. We have bee notified.")
            if (response.status == 200)
               Notiflix.Notify.Success "OK"

    $scope.cancel = !->
        $uibModalInstance.dismiss('cancel')
        ## $scope.alerts = response.data.alerts
