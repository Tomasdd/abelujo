## 0.24 (dev)

### New features

- sell: we can now generate a bill to a temporary client, a client that won't be saved in the database.
- we now can add **multiple categories** to a book, and search our stock with them. A category is independent from a shelf, we can use them in parallel.
  - you can freely create categories on the fly when you are on the book page. It's really easy to use.


### Other enhancements

- we could observe a difference of 0.01 in some price totals (like in total by payment means), it is now fixed.
- history exports: the export option that shows all the books sold was augementing: we now show the transaction ID, a column "total" that simply multiplies the quantity by the sold price, and more importantly, a column "revenue":
  - the "revenue" column is adequate to count the revenue of this month, it is adequate for accounting. This number does *not* count payments with coupons and the like.
  - reminder: this revenue number was already available in the "revenue per day" export and the "ticket Z" PDF.
- the **loading of pages was speed up**! By a factor of 2!
- other **important speed ups**:
  - selling books is 8 times faster
  - in Lists, choosing "Add to stock…" or "Add to shelf…" are 10 times faster.

### Deprecated features

- until now, we were able to create and remove places as we wished from the admin panel. There is always one default place. Now, what changes is that we must not delete this default place. We can still rename it.
  - currently, we are able to tell from which place we are selling books, but we currently do not have interesting statistics for this selling place at the end of the day.
  - we are preparing the way to have statistics for each selling place. That is, a truly useful multiple selling points feature.
  - as such: a search in the stock doesn't show *where* a book is. The card page doesn't show it.

### Admin

Deleted:

- the "command receive" and "deposit apply" API endpoints and functions were deprecated since one year and have been deleted. They now raise an error. This removes the reliance on the `django_q` and `honcho` python libraries.

## 0.23 (February, 2024)

### New features

- new stats: **stock rotation** (shelves, new stock, old stock)
- on the dashboard, we can now **click on the chart showing the age of shelves** to see the old books.
- dashboard: we show the total value of the stock, excluding the VAT and **discounted**.
- compute the (french) **provision pour dépréciation du stock**, except we don't know the scolar books, dictionaries and encyclopedia.
- **lists**: added an action to change the shelf of all the cards in this list. This adds a means to do it: one can scan books in a list and apply this action.
  - another way to do it is going to My Stock, do a search, select books and use the available action, as well as changing the shelf from the book's page.
- new datasources (experimental): Bookfinder (works somewhat).
  - Bertrand (Portugal) doesn't work any more.

### Other enhancements

- scanning an EAN10 (or 11 or 12) now works in the quicksearch field.
- a deposit is now a shelf that is marked being a deposit. That way we can use all the regular Abelujo menus (Receive, Return…).
- client page: click on the email icon to open an email to this client email address.
- fixed the bill from history when an institutional client had a discount for a sell. Don't add the discount again.
- when **bulk importing** ISBNs (with Dilicom), search for missing ISBNs in our stock: that way we can bulk-import books that are proper to our bookshop.
- history stats of each day of the month: it could show a difference with the day's history because of coupons. That's fixed.
- **baskets**: added a quickfilter to search in the current display.
  - also augmented the default page length to 500.
  - added a "command coupon" field there too.
- stats: the shelf age stat is faster.
- more work on the Institution Baskets page:
  - mark books as received
  - command the books not received
  - create bills
  - quick filter

### Admin

- new script: import clients from Librisoft
- new script: update all books in stock with Electre.
- fixed and updated dev docs.
- fabfile fixes
- logger: now with datetime
- code coverage: ignore the management commands (they only lower the stat!)

Deleted:

- the "command receive" and "deposit apply" API endpoints and functions were deprecated since one year and have been deleted. They now raise an error. This removes the reliance on the `django_q` and `honcho` python libraries.

and more!


## 0.22 (September, 2023)

### New features

- added an export option in **My Stock**, very useful for **a new kind of deposits**: we can export our stock in CSV (for Excel and LibreOffice), and choose two dates so that we get a new column in the CSV that shows the number of sells of the book between those two dates.
  - that is very useful for a new, easy and lightweight way to handle **deposits**:
    - say that all books of a given publisher are in deposits. You will register them in their own shelf. You can thus use the Reception menu as before for deposits. *Registering old books or new books for deposits is now faster than before*.
    - say that the rules to pay your deposits change for all of them. Once in a while, they will ask you two information: the quantity of their books currently in your stock, the number of sells since last time you did a checkout together. *You can now answer these two questions in a couple clicks!*.
    - go to My Stock: select a shelf corresponding to this deposit. Select Export -> CSV & sells -> select the date of start of the period, the date of end of the period (today by default), click "export".
    - you get a CSV file with the usual columns, plus a column that gives you the number of copies sold between those dates.
    - there is one caveat: a book can not be both in stock (you bought it) and in this kind of lightweight deposits.
- added a **french datasource**: "chez mon libraire".
- we can scan **ISBNs that contain dashes**, such as "978-237-9221569". It happens rarely, but sometimes.
- we can **export a new report for the month's income**: get a PDF with the total income, with each total by means of payments and VATs.
  - in French: **rapport Z**.
  - previously, you had these numbers for each day of the month, and totals in the CSV exports.
  - you can send this document to your accountant.

New charts:

- added a new menu entry to **see the total of sells per distributor**, per month.
  - you can see the importance of each distributor with an **interactive chart**. You can click each distributor in the labels to un-select it, and view the respective importance of all others without the unclicked one(s).
- on a **book history page**, we added a **chart showing the sells on a timeline**.
  - from its first sell until today, you can see at a glance how this book lived accross time. For each, see a bar representing the number of sells.
- on the **dashboard** page, we can **view the best shelves for all previous years**.
- still on the **dashboard**, we can see **a graph showing the age of shelves**
  - for each shelf, we see a pie chart that shows the repartition of the age of the books of this shelf (in percent of the shelf size)
  - if a book was acquired last year and was sold for the last time last week, it counts as not very aged (only 7 days), it it counted in the "0 to 3 months old" category.
  - however, if a book was acquired two years ago and was never sold… you might want to know this. It is counted in the "more than 24 months old" category.
  - we have categories for: 0-3 months, 3-6, 6-12, 12-18, 18-24, more than 24 months.

### Other enhancements

- we show more numbers on a **card page**:
  - the total number of sells for this book
  - the date of its last sell
  - the number of sells since the last 12 months,
  - and since the last 3 months.
- **bills**: we can add the bookstore's **logo** on bills \o/
  - see in the Preferences: logo URL. We expect you already have your logo on your website.
  - the date on PDFs is now better recognized by external tools, using a "x/y/2023" format instead of "x-y-2023".
- **bills**: when editing a bill from the lists, we added a field to enter a shipping receipt number, and a field to add a shipping cost.
- new system for **inventories**. We hide the access to the current inventories menu. Indeed, it is too old, its interface is too slow. We advertise to:
  1. contact us or your system administrator before an inventory, so we set the stock of all books to zero
  2. **use the Reception menu** to scan books again in their shelf. This menu is much more efficient.
  - the old menu is DEPRECATED. It it still accessible, but it will be deleted in January, 2024.
- **boxes**: the action "archive" does not put back the books on shelves. Use the "empty" action to cancel the current box and put everything back in place.
- **returns**: the returns created with the new Returns menu are now correctly shown in the history of a book.
  - these movements out of stock were already shown when we were using baskets and the "return to supplier" button. The returns created with the special Returns menu were not shown in the history, now they are.
- added a **new datasource for Netherlands**: naibooksellers.nl (specialized in art and architecture). Athenaeum is already an option for NL books.

### Admin

- the old inventories system is deprecated. Deletion date: beginning of 2024.
- the old deposits system is deprecated.
- new command: `compute_place_cost_discount` to get the cost of each place, minus the discount of books. As of writing, this data doesn't appear yet in the web UI.

## 0.21

February, 2023

### New features

- Stats: we show the best shelves, in percentage of the yearly revenue.
  - we can see the best shelves on the History page for the current month too.
- History: we added a button to create bills for each past sell.
- History: we added a simple to remember URL to go to today's history in one click: bookmark `/history/today`, click on your bookmark anytime and you'll be redirected to today's history.
  - we added this link in som places, such as the Sell page and the navbar menu.
- History: we added a calendar to choose precisely what day to see the history of, and in the global month view, we can choose the year and the month.
- History: we made the history pages load faster (without a page refresh) when you click on "previous" or "next".
- we added a **Returns menu**, with the auto-created boxes for each supplier. The boxes menu is for user-created lists.
  - there is a default Returns list, where we can scan any book and it will be added to its corresponding supplier list.
- Pages menu: added a page to list all unsold cards.
- Card page: we can click on the cover image to enlarge it in a modale window. Click next to the image to quit the modale.

### Other enhancements

- **click on the images** to enlarge them!
- the admin page of a Card was completely translated! We found out why some where missing until now.
- Bills: we fix the case when we made the bill for an institutional client, who gets an automatic discount, and where the price before taxes ("prix HT") was not calculated right and was higher than the final price to pay.
- Sells: after you selected a client, use the TABULATION key to go back to the books search input.
- Lists:
  * the created date of the card is replaced by the publication date of the book.
  * the dates are shorter, we don't need to see the hour.
  * did you know that you can add **comments to a list**? Especially handy to remember things or to work with another person. But the comments did not show up by default. That's fixed. Also they appear completely when you hover the mouse over a list name.
  * the **presentation of search results is greatly improved**: we show the price, the format (Pocket), the cover.
- History: its presentation was improved to show the same data in less space, so it shows better on small to medium screens.

### Admin

- more API endpoints are secured by an auth token if you use the FEATURE_USE_API_TOKEN setting. On case of an auth error, users see an orange notification popup.

## 0.20

### New features

- **custom client pages** with useful data.
- quicksearch: the quicksearch input in the navigation now allows to search for clients: type a question mark followed by a text: `?vinc` will find a client named Vincent.
- the Box menu, used for returns to suppliers, now will automatically create an entry for each supplier of the database (if this supplier has books in our stock).
- Boxes: if we add a book in a box list, specified with a given supplier, but this book has a different one, we display a notification in orange, and we highlight the different suppliers in orange in the page.

<!-- oops, that's in french -->
<!-- - le menu "Cartons et Retours", qui sert pour les retours, crée maintenant automatiquement une entrée pour chaque distributeur présent en base de données. -->
<!-- - Retours: dans une liste de retours (Cartons), si on ajoute un livre dans un carton qui n'est pas associé au même distributeur, un message de notification est montré en orange et les distributeurs différents sont surlignés en orange sur la page. -->

### Other enhancements

- the Abelujo views and its API fire less requests to external services.
  The goal is to minimize them, because Dilicom is only 40,000 requests per year.
  - the sell view does not fire an update request anymore. It was done to get a price update, but: it is seldom needed by users and it makes too many requests in total. Will exhaust the providers' plan. There is a backend setting to set this back.
- add a 2s timeout to all data sources. Otherwise a Dilicom outage can slow Abelujo.
- the quicksearch bar accepts a `dist:` filter. It used to work only with a distributor ID (for internal purposes), it now accepts a string, to filter books by this distributor name.
  - we can use several filters at the same time, for example: `dist:inter aut:mil achi` will find a book distributed by Interforum, whose author name contains "mil" (it finds Miller Madeline) and whose title contains "achi" (it finds "Achille").

### Admin

- new setting: `FEATURE_UPDATE_CARD_ON_SELL=False`: set to True to fire a vendor request when we sell a card. If the price changes, the sell page warns the user and shows the previous price.
- a remainder: delete the old cover images saved in `collectedstatic/images/covers/`, we don't use downloaded images anymore and they can get into the way.

## 0.19.1

### New features

- History CSV export: added the VAT
- on the **Client admin page**, we can select clients and **download the clients data in CSV**.
- on a Card page, we added a quick client to the card's admin, so we can edit it quickly.
- fix: Stock page, handle the date created filter.
- more Stripe API improvements, notably ability to commands books that are not in the DB yet (in order to work with websites that have an Electre search).
- we added a VAT field on CardType objects: now we can create other type of products than books AND tell what's their VAT. Before this, Abelujo relied on some heuristics to find the VAT.
  - so, exporting the history in CSV shows the VAT of all products.

### Other enhancements

- **Commands**: the command/reservation popub window from a client page was missing the possibility to choose the quantity to reserve. It was 1 by default. It is now fixed.
- **Card page**: a click on the distributor now searches the stock for cards of this supplier, it doesn't open the admin panel for that distributor.
- **websites**: Abelujo can now take commands from a distant website for books that it doesn't have in stock. That adds them on the command list. Needs Dilicom (for the bulk update).

### Admin

- `dbback` zips the DB (>90% size gains)
- **important**: for the interactive search of clients to continue to work, you must run a little script manually:

    $ ./manage.py shell_plus
    >>> for cl in Client.objects.all(): cl.save()

This is only required if you are upgrading from an existing installation, not if you install the software.

## 0.18.3

### New features

- **bulk import** of ISBNs with (optional) their associated quantity. Accepts the CSV format or a list of ISBNs.
  - from the Lists menu, click on "Import…"
- support for **Stripe payments**, sent from a front website.
  - with validation emails (to the client and the show owner)
  - shipping costs
  - books bought online create **reservation objects** on Abelujo side, then the show owner validates the sell (or not).
  - support for **custom email templates**.
- **user permissions**: non-staff members can only see the stock. They can:
 - see the stock
 - search for books
 - see a books' page
 - see the history
 - see client reservations
 - see special pages (bookshop selection for website)
 - => Create a new user from the admin panel, don't give it any rights.
 - Added a button to **quickly command from search results**.
   - this is handy, but keep in mind: it is slower than scanning ISBNs, and the free search is not endorced by Dilicom's FEL à la demande or Electre's API. The results may vary between Abelujo and those platforms.
   - that being said, before saving and commanding the book, we do rightfully fetch the full book data on Dilicom or Electre (given you subscribed to one of them).
- **Sell**: added the ability to enter a purchase order ID, to show on a PDF bill and on a sell. We can see this ID in the history of a day.
- **My stock**: added ability to filter by a Dilicom distributor. This presents all distributors known by Dilicom, not only the ones we have in our DB.
- **My stock**: added button to quickly reserve a book for a client (just as in the Search page results).
- **History**: export the sells with a new **CSV** format: **the total revenue for each day**, one line per day.
- added a **lang** field.

### Other enhancements

- Abelujo was translated to **Occitan** (thanks to La Tuta d'Oc bookstore)
- **Sell: import a client's reservations** (the ones we received, but import all of them anyways if needed).
- Other sell enhancements:
  - show the **change amount** to give back. For it to show, enter an amount in the first "pay" box greater than the total price. You will see the change amount below appear instead of a second payment method.
  - importing a client's commands doesn't replace the selection.
  - more pointer focus: removing a title with a mouse click puts the cursor back to the search box.

- searching our **stock**: more sort order options (sort by creation date of the book, by title (anti) alphabetically, by price, by shelf, by publisher.
- **Sell bill**: specify a command number to display on the generated PDF bill.
- **History**: we display the actual quantity in stock next to the command button.
- **Search**: shows the **distributor** name in a new column.
  - **Search**: can use Electre's API.
  - if Electre is not available, we can only show the distributor name of a book we already have in stock. Without Electre, the distributor is *not* given by the search results (and keep in mind that Dilicom's FEL à la demande doesn't provide a free text search).
  - when adding a book to our stock after a free text search, using a web scraper, and if Dilicom is enabled, we update the book data (so we get the missing distributor information). If we have the Electre API, we already have all the data.
- **Card**: on a card page, clicking on the author(s) now searches in our stock.
  - and clicking on the publisher too.
- **Reception**: the list of shelves is shown with an alternating white/grey color background, in order to ease reading and selecting a shelf.
- Book page: if the book has no shelf, show a blank option in the select button. Before, we were showing the first shelf of our database, wrongly, that was misleading.
- **Suppliers**: added a field to add our client number specific to this supplier. This number should appear in PDF exports.

### Admin

- it is possible to theme templates, especially client confirmation emails. See `settings.EMAIL_THEME`. Theme templates should be located in `templates/theme/<theme name>/`.
- new script: `manage.py export_stock --format csv` to create a csv file from all cards in stock and with a quantity > 0. Because doing that from the UI can be too long.
- the strategy to handle reservations changed a bit. When we reserve a book not in stock, we don't do a -1 to the stock anymore. If we reserve one we do have in stock, we do the -1. This works better for the main use case of this fonctionnality. The admin can run the script `fix_negative_reservations`.
- added token authentication for API endpoints. See the developer documentation. It is currently not set by default, for compatibility of existing web clients.


## v0.18 (2020-12, 2021-09)

yes it's a +3 version jump because we are late and have 3 big new features!

### New features

- new **Reception menu**: receive books, scan them in a row and register them quickly in the appropriate shelf (if they don't have one already).
- new **Client commands**: register commands for clients from the book view or from the search view. See them all, send an email to the client.
  - if the book is not in stock, show it in the command list.
- Abelujo now supports the **Electre API** as an alternative to Dilicom's FEL à la demande (the FEL Onix complet is being worked on).
- new special page to list books to *not* show on the frontend website (exclude them from the API). This feature must be enabled, see in admin.
- **Sell**: support to **print sell receipts** with the USB thermic receipt printer [Epson TM-20](https://www.amazon.fr/gp/product/B07Y59V9LP/ref=as_li_tl?ie=UTF8&camp=1642&creative=6746&creativeASIN=B07Y59V9LP&linkCode=as2&tag=abelujo-21&linkId=b3938b7d12f66b76d2d9e1c5d6f6e8b7). Previously, you could print a bill. The process is now must faster.
  - install the printer
  - print a test page
  - before validating a new sell, ask your browser to print the current page and choose the ticket printer.

- Baskets: added an action to add all the cards to the stock, without having to choose a shelf. This greatly **speeds up the registration of new books**.
- We added a global parameter to **use CLIL themes for the shelf** name,
  in order to speed up the registration of new books (this removes the
  only required manual step until now). We can use the new action
  above, and still have shelves for our books.
- History: added a button to quickly **command** a book (add it into the command list).
- History: the history gets more **breakdown**: it shows totals for each type of product (book, CD, beverage…)
- History: the history page of the month and the day show the **best sells** for each kind of product: books, but not only.
- Sell page: we can pay with **two payment methods**.
- Sell page: print adapted to print **receipts** (tested with Epson TM-20)
- the Commands page shows **all cards to command**, in addition to them grouped by supplier. This makes it easier to download a CSV and send it to a supplier.
- added a button to collapse the left menu. We can now work in "full screen". Especially useful for the Raspberry Pis and views with a lot of information (Reception, lists…).
- We can choose what books to show on the ABStock catalogue from within Abelujo (see new page `/catalogue-selection`).
- We can now see the past inventories.
- quick search bar: we introduce a way to filter our search. Type "keyword ed:foo" (`ed:foo` is important here) to filter the results by publisher containing "foo" in its name.

More data delivered with the API:

- say if a book was found on Dilicom or not (`dilicom_unknown_ean`), on `/api/card/:id?with_dilicom_update=1`.

### Other enhancements

- **Bills** enhancements (added a unique number, better TVA details, other fixes).
- on a card page, the cart button to add a card to the command list was fixed to add 1 copy at each click (instead of adding the book, but not updating the quantity to command).
  - in addition, the button also shows the quantity in the command list.
- new **Preference setting**: choose wether or not to automatically add a card to the list of commands after a sell (if its quantity in stock gets lower its minimal treshold).
  - **changed**: this features is now disabled by default.
- when we have more than one places in the database, allow to choose where we sell books from in the **sell page** (this was removed seven months ago, it is back but hidden by default).
- during a sell, we warn the user if a price of a book that is being sold was updated (it can be updated asynchronously, just after it was added to the sell).
- we do not emphasize about many (selling) places anymore. It adds too much complexity and was seldom used. Now Abelujo expects one stocking place and one selling place.
  - you can still create other (selling) places, it mostly works, but some details are lacking. You might get some confusion (but no data loss).
- fixed the broken redirection after a logout and new login.
- **speed up** improvements for the history display (more than 10x).
- **speed up** improvements to the CSV and TXT download of history logs (30%).
- deposits: we don't mention anymore the type of deposits that are sent by us to the exterior world.
- **speed up** to display the list of commands.
- **speed up** of the analysis of inventories (x10, but not always at the first access).
- **speed up** of the inventory analysis page.

### For administrators

- **ease of installation**: the installation requires many fewer steps and dependencies, see below.
- new feature flag: `FEATURE_EXCLUDE_FOR_WEBSITE` (defaults to False): exclude books from the API to not show them on the frontend website.
- new feature flag: `FEATURE_ELECTRE_API` (False), as well as the related password and username settings.
- new feature flag: `FEATURE_SHOW_RESERVATION_BUTTON`, defaults to `True`: show a reservation button on the card page (reserve for a client).
- new script: `./manage.py move_from_place --origin <id>` to move all cards from one place to another (the destination defaults to the default place). It might be useful, because Abelujo doesn't encourage to use more than one selling place anymore.
- new script: `./manage.py get_higher_covers`: replace book covers (gotten from librairie-de-paris) to higher resolution ones. To run once.

### Upgrade instructions

- Abelujo doesn't need npm or Node packages any more for its installation, making the installation and updates much easier.
- Nothing special, but it is recommend to run the `update_all_with_dilicom` script.
- see the preferences page and check "auto_command_after_sell" to get the old behaviour.
- optional: clean up cards' types with `fix_types` (set their type correctly to "book" if they have a book ISBN).

## v0.13.3 (2020-09, 2020-11)

### New features

- new **boxes** menu. It's like the lists, except that adding a book in a "box" removes it from our stock. This is particularly useful when working with commands. We take a book from the shelf, put it in the parcel, and the quantity in stock reflects the reality. Before, we had to validate the list.
- in the **history**: we now differentiate the revenue from books and other products, that have a different VAT. Necessary for accounting.
- we can **generate a bill for a list** and **generate a bill during a sell**.
  If the client has a default discount, apply it.
- we can **sell a list**.

### Enhancements

- adding a book in a **list** always moves it at the top of the list. Before, there were cases where an existing book would not move, so we could not see its new quantity easily.
- lists: sort books by last modified date. This allows to see the last books we added to a list.
- we added a button on the card page to quickly **add this book to the command list**. Previously, books were added to the command list when they were sold and their quantity got below a given treshold. There was no manual way, this is now fixed.
- **automatic commands**: when a book is added to the list of books to command, its quantity to command is not set to 1 (or more) anymore, but 0. That way, the booksellers will /select/ what books to command and how much, instead of /un-selecting/ them.

### Fixes

- baskets: there was an issue in the rendered HTML that prevented from sorting the table by title and publisher. It is now fixed.


## v0.13 (2020-07, 2020-06, 2020-05)

### New features

New **bibliographic and stock** features:

- **searching the stock** now correctly ignores accents: searching
  "stromquist" also returns "strömquist".
- My Stock: we can **filter results by date of creation** of the card in
  our database. We choose an operator like "<=" and we write a date
  indicator, like: "june, 2020", "1st may 2016", etc. For details: see the
  `dateparser` library, and the online documentation.
- we get more fields from Dilicom: the **distributor GLN and the theme**.
- My Stock: we can select books and assign them to another shelf.
- on a card page, we can **change the shelf quickly** (a select field is present).

New **sell** features:

- new "coupon" payment method. The sell appears in the history but is
  not counted in the day's revenue. Indeed, the coupon was sold before
  and we must not count the sell twice. More about coupons and sell
  options are coming.
- new possibility to return a book during the sell, by inputting "-1" in the quantity field.
- if an ISBN is scanned but not found (in the stock or on Dilicom), warn the user with a yellow message, and allow to scan other books.

New **clients** features:

- create clients
- during a sell: choose a client or create one on the fly with the "+" button.
  - generate a bill of this sell for this client, in PDF.
  - on validation, the client is registered for this sell. On the
    history, we view the clients.

New **deposits** features:

- added: we display a missing message to the user when a deposit of that name already exists.
- added a "create" button on a deposit page.
- changed: when creating a new deposit the distributor is now optional
- changed: go to the deposit's page after creation success, not the list of deposits.


### Other enhancements

- The admin page for shelves shows how many books each shelf contains.
- we can now easily configure payment methods in a configuration
  file. See the developper documentation, "installation" page.
- creating a command (with the OK button) was dramatically sped up
  (from a few seconds to a fraction of a second).
- selling a card that is in a deposit automatically sells it from the
  deposit. Previously, we had to be explicit in the sell UI.
- the default timezone was set to Paris (UTC+1/+2), preventing some
  date inconsistencies.

**Speed ups**:

- faster loading My Stock page: we don't display the newest books at the
  landing page anymore.
- searching cards by keywords is nearly 2 times faster.


### Admin

New admin scripts (all to be run with `./manage.py <script>`):

- `gen_ascii_fields`: command to run in order for the search to work
  regardless of accents. This creates new database fields, the title
  and authors names in ascii, without accentuated letters, which are
  also used during the search.
- `update_all_with_dilicom`: update ALL the cards with Dilicom
  data. Update the publisher (to have one unique name across the
  application), the theme, the distributor GLN, and all.
- `import_csv_with_dilicom`: batch-import a CSV (ISBN;quantity) with
  Dilicom.
- `remove_unused_publishers`, useful after the above script.
- `list_publishers` and do nothing.

## Upgrade instructions

As usual (make update).

Run the `gen_ascii_fields` script.

If you use Dilicom, you'll certainly want to run a couple of scripts above.


## 2020-01…04

### New features

- we handle a Swiss books datasource.
- hence, we handle different currencies (euro "10€", swiss franc "CHF 10")
- in baskets: new action "add to shelf…" that empties the basket so
  than we can do it again for another shelf. Useful when receiving
  books.
- TODO

## 2019-XX

TODO


## 2019-02

### Fixes and enhancements

- the Commands view was too slow and the pages were changed.
  * we now have an index listing all the suppliers that have some cards to command, and each has its own Command page.
  * the bottleneck function at the problem origin (computing the total
    quantity of a given card in all places), also impacting other
    pages, was dramatically improved (from minutes to 5 seconds).

## 2018-12

### New Features

Return a list to its supplier.

- in a list, we can choose the action "return to supplier…":
  - every book is removed from the stock with the corresponding quantity
  - it creates an "Out Movement", for history
  - we can see the movement in a card's history


## 2018-11

### New Features

- Import a csv file with an ISBN and a quantity with a manage.py
command. See developer documentation.


# v0.6 2018-11-16

### Fixes

- pinned more dependencies (whitenoise)


# v0.5

## 2018-10

### New Features

Deposits rewrite:

- the Deposits section was re-written.


## 2018-09

### New features

- a list/basket can be linked to a supplier, so than further actions
  (apply to the stock, receive a parcel,…) can set this supplier for
  all the cards.
