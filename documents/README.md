
# Dilicom distributors

Last update: 2023-09-15

We must manually get a list from Dilicom's website.

- log into Dilicom's website
- Liens Pratiques > Annuaire des Éditeurs et des Distributeurs
- choisir éditeurs, "Tous"
  - (attention longue recherche, téléchargement de la liste peut échouer, s'y reprendre à deux fois)
- exporter la liste (-» fichier .slk)
- supprimer les 4 premières lignes d'en-tête, pour que le fichier commence avec les données.

    3010955600100	HACHETTE LIVRE	78310	MAUREPAS	FRANCE	610679	O

Les colonnes sont: GLN, nom, code postal, ville, pays, nb de titres dans le catalogue, commandable via Dilicom (Oui).

- remplacer les accents: é -> e, Ê -> E et les mauvaises backquotes: ’ -> ' parce que fuck it
- save the file into `annuaire_distributeurs.csv`, with ";" field separator, UTF-8 file format
- vérifier que `make run` fonctionne et charge le fichier.

It is specially used in the function `Card.from_dict`, when we have
search results from Dilicom and create a Card object. The results, for
each book, have a GLN number for the distributor. We want to know its name.

Notes:

- we don't need the column "number of books in Dilicom" and it is responsible for a big file diff.
